﻿using dahua.Model;
using dahua.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace dahua.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class DahuaController : ControllerBase
    {
        private readonly IDahuaService _dahuaService;

        public DahuaController(IDahuaService dahuaService)
        {
            this._dahuaService = dahuaService;
        }

        [HttpGet("device")]
        public IActionResult GetDevice()
        {
            List<Device> result = _dahuaService.getDevice();
            return Ok(result);
        }

        [HttpGet("alarm")]
        public IActionResult GetAlarm()
        {
            List<Alarm> result = _dahuaService.getAlarm();
            return Ok(result);
        }

        [HttpGet("alarm/count")]
        public IActionResult GetAlarmCount()
        {
            AlarmCount result = _dahuaService.getAlarmCount();
            return Ok(result);
        }

        [HttpGet("alarm/type")]
        public IActionResult GetAlarmType()
        {
            string result = _dahuaService.getAlarmType();
            return Ok(result);
        }
    }
}