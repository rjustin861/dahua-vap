﻿using dahua.Repository;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace dahua.Hubs
{
    public class DahuaHub : Hub
    {
        private readonly IDahuaEventHandler _dahuaEventHandler;

        public DahuaHub(IDahuaEventHandler dahuaEventHandler)
        {
            _dahuaEventHandler = dahuaEventHandler;
        }

        public async Task SendMessage(string command, string user, string message)
        {
            await _dahuaEventHandler.sendMessage(command, user, message);
        }
    }
}
