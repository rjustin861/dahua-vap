﻿namespace dahua.Model
{
    public class Alarm
    {
        public string AlarmId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string ChannelId { get; set; }
        public string ChannelName { get; set; }
        public string HandleUser { get; set; }
        public string HandleTime { get; set; }
        public string HandleMessage { get; set; }
        public string AlarmCode { get; set; }
        public string AlarmTime { get; set; }
        public string AlarmGrade { get; set; }
        public string AlarmType { get; set; }
        public string AlarmStatus { get; set; }
        public string HandleStatus { get; set; }
        public string AlarmPicture { get; set; }
        public string AlarmPictureSize { get; set; }

    }
}
