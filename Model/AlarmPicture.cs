﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dahua.Model
{
    public class AlarmPicture
    {
        public string AlarmCode { get; set; }
        public string AlarmTime { get; set; }
        public string AlarmPic { get; set; }
        public uint AlarmPicSize { get; set; }
    }
}
