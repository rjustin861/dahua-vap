﻿namespace dahua.Model
{
    public class Device
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public string IP { get; set; }
        public uint Port { get; set; }
        public int Status { get; set; }
    }
}
