﻿namespace dahua.Model
{
    public class Face
    {
        public string ChannelId { get; set; }
        public int FaceId { get; set; }
        public string FaceUrl { get; set; }
        public bool IsHit { get; set; }
        public string PictureUrl { get; set; }
        public int IRecAge { get; set; }
        public int IRecExpress { get; set; }
        public int IRecFringe { get; set; }
        public int IRecSex { get; set; }
        public int IRecGlasses { get; set; }
        public int IRecEmotion { get; set; }
        public int IAppearTimes { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public uint SimilarFace { get; set; }
    }
}
