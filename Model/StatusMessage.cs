﻿namespace dahua.Model
{
    public class StatusMessage
    {
        public string Status {get; set;}
        public string Message {get; set;}

        public StatusMessage() { }

        public StatusMessage(string status, string message)
        {
            this.Status = status;
            this.Message = message;
        }
    }
}
