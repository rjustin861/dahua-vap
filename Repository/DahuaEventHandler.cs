﻿using dahua.Hubs;
using dahua.Model;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static dahua.Repository.DahuaRepository;

namespace dahua.Repository
{
    public class DahuaEventHandler: IDahuaEventHandler
    {
        private readonly ILogger<DahuaEventHandler> _logger;
        private IHubContext<DahuaHub> Hub { get; set; }

        public DahuaEventHandler(ILogger<DahuaEventHandler> logger, IHubContext<DahuaHub> hub)
        {
            this._logger = logger;
            this.Hub = hub;
        }

        public async Task sendMessage(string command, string user, string message)
        {
            await Hub.Clients.All.SendAsync(command, user, message);
        }

        public void eventCallback(IntPtr iEventType, IntPtr pEventBuf, IntPtr uiBufSize, IntPtr pUserData)
        {
            string strNotify = "";
            _logger.LogDebug("==== Event Type : " + iEventType.ToInt32() + " ====");
            switch (iEventType.ToInt32())
            {
                case DPSDK_EVENT_SERVER_OFFLINE:
                    strNotify = "===== Server offline =====";
                    break;
                case DPSDK_EVENT_RELOGIN_SUCCESS:
                    strNotify = "===== Server relogin success =====";
                    break;
                case DPSDK_EVENT_ALARM_ALARMEVENT:
                    strNotify = "===== Alarm Event Notify success =====";
                    AlarmEventNotify(pEventBuf);
                    break;
                case DPSDK_EVENT_ALARM_CONFIRMALARM:
                    strNotify = "===== Alarm Confirm Alarm success =====";
                    //AlarmConfirmNotify(pEventBuf);
                    break;
                case DPSDK_EVENT_ALARM_ALARMPICTURE:
                    strNotify = "===== Alarm Picture success =====";
                    AlarmPictureNotify(pEventBuf);
                    break;
                case DPSDK_EVENT_ALARM_EXPORTALARM:
                    strNotify = "===== Alarm Export success =====";
                    //AlarmExportNotify(pEventBuf);
                    break;
                case DPSDK_EVENT_DEVICE_STATUS:
                    strNotify = "===== Event device status success =====";
                    DPSDK_DEV_STATUS_NOTIFY struDevStatusNotify = new DPSDK_DEV_STATUS_NOTIFY();
                    struDevStatusNotify = (DPSDK_DEV_STATUS_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_DEV_STATUS_NOTIFY));
                    //strNotify = "\r\nDevice status modify. deviceCode = " + struDevStatusNotify.szDeviceID + ", device status = " + struDevStatusNotify.iStatus.ToString();
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_CHANNEL_STATUS:
                    strNotify = "===== Event channel status success =====";
                    DPSDK_CHANNEL_STATUS_NOTIFY struChannelStatusNotify = new DPSDK_CHANNEL_STATUS_NOTIFY();
                    struChannelStatusNotify = (DPSDK_CHANNEL_STATUS_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_CHANNEL_STATUS_NOTIFY));
                    //strNotify = "\r\nChannel status modify. channelCode = " + struChannelStatusNotify.szChannelID + ", channel status = " + struChannelStatusNotify.iStatus.ToString();
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_ADD_ORG:
                    strNotify = "===== Event add org success =====";
                    DPSDK_ORG_BASE_INFO struOrgBaseInfo = new DPSDK_ORG_BASE_INFO();
                    struOrgBaseInfo = (DPSDK_ORG_BASE_INFO)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_ORG_BASE_INFO));
                    string strOrgName = Encoding.GetEncoding("utf-8").GetString(Encoding.Default.GetBytes(struOrgBaseInfo.szOrgName));
                    //strNotify = "\r\nAdd org. org code = " + struOrgBaseInfo.szOrgCode + ", org name = " + strOrgName;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_MODIFY_ORG:
                    strNotify = "===== Event modify org success =====";
                    DPSDK_ORG_BASE_INFO struModifyOrgBaseInfo = new DPSDK_ORG_BASE_INFO();
                    struModifyOrgBaseInfo = (DPSDK_ORG_BASE_INFO)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_ORG_BASE_INFO));
                    string strModifyOrgName = Encoding.GetEncoding("utf-8").GetString(Encoding.Default.GetBytes(struModifyOrgBaseInfo.szOrgName));
                    //strNotify = "\r\nModify org. org code = " + struModifyOrgBaseInfo.szOrgCode + ", org name = " + strModifyOrgName;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_DELETE_ORG:
                    strNotify = "===== Event delete org success =====";
                    //strNotify = "\r\nDelete org, xml = " + Marshal.PtrToStringAnsi(pEventBuf);
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_MOVE_ORG:
                    strNotify = "===== Event move org success =====";
                    DPSDK_MOVE_ORG_NOTIFY struMoveOrgNotify = new DPSDK_MOVE_ORG_NOTIFY();
                    struMoveOrgNotify = (DPSDK_MOVE_ORG_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_MOVE_ORG_NOTIFY));
                    //strNotify = "\r\nMove org, old org code = " + struMoveOrgNotify.szOldOrgCode + ", new org code = " + struMoveOrgNotify.szNewOrgCode;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_ALERT_USER:
                    strNotify = "===== Event alert user success =====";
                    //strNotify = "\r\nalert user.";
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_ADD_DEVICE:
                    strNotify = "===== Event add device success =====";
                    DPSDK_ADD_DEVICE_NOTIFY struAddDevcieNotify = new DPSDK_ADD_DEVICE_NOTIFY();
                    struAddDevcieNotify = (DPSDK_ADD_DEVICE_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_ADD_DEVICE_NOTIFY));
                    string strAddDevcieDeviceName = Encoding.GetEncoding("utf-8").GetString(Encoding.Default.GetBytes(struAddDevcieNotify.struDevAllInfo.struDevInfo.szDeviceName));
                    //strNotify = "\r\nAdd device, org code  = " + struAddDevcieNotify.szOrgCode + ", device ID = " + struAddDevcieNotify.struDevAllInfo.struDevInfo.szDeviceID + ", device name = " + strAddDevcieDeviceName;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_MODIFY_DEVICE:
                    strNotify = "===== Event modify device success =====";
                    DPSDK_MODIFY_DEVICE_NOTIFY struModifyDevcieNotify = new DPSDK_MODIFY_DEVICE_NOTIFY();
                    struModifyDevcieNotify = (DPSDK_MODIFY_DEVICE_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_MODIFY_DEVICE_NOTIFY));
                    string strModifyDevcieDeviceName = Encoding.GetEncoding("utf-8").GetString(Encoding.Default.GetBytes(struModifyDevcieNotify.struDevAllInfo.struDevInfo.szDeviceName));
                    //strNotify = "\r\nModify device, old org code  = " + struModifyDevcieNotify.szOldOrgCode + ", new org code = " + struModifyDevcieNotify.szNewOrgCode + ", device ID = " + struModifyDevcieNotify.struDevAllInfo.struDevInfo.szDeviceID + ", device name = " + strModifyDevcieDeviceName;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_DELETE_DEVICE:
                    strNotify = "===== Event delete device success =====";
                    DPSDK_DELETE_DEVICE_NOTIFY struDeleteDeviceNotify = new DPSDK_DELETE_DEVICE_NOTIFY();
                    struDeleteDeviceNotify = (DPSDK_DELETE_DEVICE_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_DELETE_DEVICE_NOTIFY));
                    //strNotify = "\r\nDelete device, org code  = " + struDeleteDeviceNotify.szOrgCode + ", device ID = " + struDeleteDeviceNotify.szDeviceID;
                    //ShowEventNotify(strNotify);
                    break;
                case DPSDK_EVENT_MOVE_DEVICE:
                    strNotify = "===== Event move device success =====";
                    int iCount = (int)uiBufSize / Marshal.SizeOf(typeof(DPSDK_MOVE_DEVICE_NOTIFY));
                    for (int i = 0; i < iCount; i++)
                    {
                        IntPtr ptr = new IntPtr(pEventBuf.ToInt32() + i * Marshal.SizeOf((typeof(DPSDK_MOVE_DEVICE_NOTIFY))));
                        DPSDK_MOVE_DEVICE_NOTIFY struMoveDeviceNotify = new DPSDK_MOVE_DEVICE_NOTIFY();
                        struMoveDeviceNotify = (DPSDK_MOVE_DEVICE_NOTIFY)Marshal.PtrToStructure(ptr, typeof(DPSDK_MOVE_DEVICE_NOTIFY));
                        //strNotify = "\r\nMove device, old org code  = " + struMoveDeviceNotify.szOldOrgCode + ", new org code = " + struMoveDeviceNotify.szNewOrgCode + ", device ID = " + struMoveDeviceNotify.szDeviceID;
                       // ShowEventNotify(strNotify);
                    }
                    break;
                case DPSDK_EVENT_VTCALL_INVITE:
                    strNotify = "===== Event vtcall invite success =====";
                    break;
                case DPSDK_EVENT_MEDIA_SCREENSHOT:
                    strNotify = "===== Event media screenshot success =====";
                    break;
                case DPSDK_EVENT_FACE_INFO:
                    strNotify = "===== Face recognition information success =====";
                    FaceEventNotify(pEventBuf);
                    break;
                default:
                    break;
            }
            _logger.LogDebug(strNotify);
        }

        private void AlarmEventNotify(IntPtr pEventBuf)
        {
            DPSDK_ALARMEVENT_NOTIFY struNotify = (DPSDK_ALARMEVENT_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_ALARMEVENT_NOTIFY));

            _logger.LogDebug("AlarmCode=" + struNotify.szAlarmCode + " AlarmNodeCode=" + struNotify.szAlarmNodeCode + " AlarmTime=" + struNotify.szAlarmTime + " AlarmGrade=" + struNotify.iAlarmGrade.ToString() +
            " AlarmStatus=" + struNotify.iAlarmStatus.ToString() + " AlarmObjType=" + struNotify.iAlarmObjType.ToString() + " AlarmType=" + struNotify.iAlarmType.ToString() + " AlarmCategory=" +
            struNotify.iAlarmCategory.ToString() + " AlarmMessage=" + struNotify.szAlarmMessage);
        }

        private async void FaceEventNotify(IntPtr pEventBuf)
        {
            DPSDK_FACE_INFO_NOTIFY struNotify = new DPSDK_FACE_INFO_NOTIFY();

            struNotify = (DPSDK_FACE_INFO_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_FACE_INFO_NOTIFY));

            _logger.LogDebug("Channel ID=" + struNotify.szChannelID + " Face ID=" + struNotify.IFaceImageId.ToString() + " Face Image URL=" + struNotify.SzFaceImageUrl + " Is Hit=" + struNotify.BHited.ToString() +
            " Picture URL=" + struNotify.SzPictureUrl + " Identification Age=" + struNotify.IRecAge.ToString() + " Distinguish Express=" + struNotify.IRecExpress.ToString() + " IRecFringe=" +
            struNotify.IRecFringe.ToString() + " Identification of sex=" + struNotify.IRecSex.ToString() + " Identification of glass=" + struNotify.IRecGlasses.ToString() + " Identification of emotion=" + struNotify.IRecEmotion.ToString() +
            " Historical occurences=" + struNotify.IAppearTimes.ToString() + " View Time=" + struNotify.SzBeginTime + " Departure Time=" + struNotify.SzEndTime + " Number of similar face=" + struNotify.UiSimilarFaceListSize.ToString());

            Face face = new Face()
            {
                ChannelId = struNotify.szChannelID,
                FaceId = struNotify.IFaceImageId,
                FaceUrl = struNotify.SzFaceImageUrl,
                IsHit = struNotify.BHited,
                PictureUrl = struNotify.SzPictureUrl,
                IRecAge = struNotify.IRecAge,
                IRecExpress = struNotify.IRecExpress,
                IRecFringe = struNotify.IRecFringe,
                IRecSex = struNotify.IRecSex,
                IRecGlasses = struNotify.IRecGlasses,
                IRecEmotion = struNotify.IRecEmotion,
                IAppearTimes = struNotify.IAppearTimes,
                BeginTime = struNotify.SzBeginTime,
                EndTime = struNotify.SzEndTime,
                SimilarFace = struNotify.UiSimilarFaceListSize
            };

            await sendMessage("facialRecognitionEvent", "server", JsonConvert.SerializeObject(face));
        }

        private async void AlarmPictureNotify(IntPtr pEventBuf)
        {
            DPSDK_ALARM_DETAILINFO_NOTIFY struNotify = new DPSDK_ALARM_DETAILINFO_NOTIFY();

            struNotify = (DPSDK_ALARM_DETAILINFO_NOTIFY)Marshal.PtrToStructure(pEventBuf, typeof(DPSDK_ALARM_DETAILINFO_NOTIFY));

            _logger.LogDebug("AlarmCode=" + struNotify.szAlarmCode + " AlarmTime=" + struNotify.szAlarmTime + " AlarmPicture=" + struNotify.szAlarmPicture + " PictureSize=" + struNotify.uiAlarmPictureSize.ToString());

            AlarmPicture alarmPicture = new AlarmPicture()
            {
                AlarmCode = struNotify.szAlarmCode,
                AlarmTime = struNotify.szAlarmTime,
                AlarmPic = struNotify.szAlarmPicture,
                AlarmPicSize = struNotify.uiAlarmPictureSize
            };

            await sendMessage("alarmPictureEvent", "server", JsonConvert.SerializeObject(alarmPicture));
        }
    }
}
