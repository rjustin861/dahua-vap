﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using DPSDK_RES = System.Int32;

namespace dahua.Repository
{
    public class DahuaRepository
    {
        /// <summary>
        /// necessary designer
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// clear all resource
        /// </summary>
        /// <param name="disposing">release resource=true; other=false。</param>
        public void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
        }

        private IntPtr m_iSessionID;
        private IntPtr m_iPreviewMediaSessionID;
        private IntPtr m_iPlaybackMediaSessionID;
        private IntPtr m_iDownloadMediaSessionID;
        private int m_iConvertType;
        private Dev_Info_All m_struDevInfoAll;
        private Dep_Info_All m_struDepInfoAll;
        private Dictionary<string, string> m_mapIdName;
        private int m_iAlarmExportSessionID;
        public EventCallBack m_fEventCallbackFun;
        public DPSDK_EVENT_DOWNLOAD_CALLBACK m_fEventDownloadCallback;

        public const int DPSDK_SUCCESS = 0;        // Success
        public const int DPSDK_EVENT_SERVER_OFFLINE = 1;        // Service offline
        public const int DPSDK_EVENT_RELOGIN_SUCCESS = 2;		// Service reconnection successfully
        public const int DPSDK_EVENT_ALARM_ALARMEVENT = 3;        // Alarm event
        public const int DPSDK_EVENT_ALARM_CONFIRMALARM = 4;		// Alarm confirmation
        public const int DPSDK_EVENT_ALARM_ALARMPICTURE = 5;		// Alarm linked picture
        public const int DPSDK_EVENT_ALARM_EXPORTALARM = 6;		// Alarm export
        public const int DPSDK_EVENT_DEVICE_STATUS = 7;		// Device status change
        public const int DPSDK_EVENT_CHANNEL_STATUS = 8;		// Channel status change
        public const int DPSDK_EVENT_ADD_ORG = 9;		// Add organization
        public const int DPSDK_EVENT_MODIFY_ORG = 10;		// Modify organization
        public const int DPSDK_EVENT_DELETE_ORG = 11;		// Delete organization		The style of data is xml <NodeIDs><NodeID>OrganizationID</NodeID></NodeIDs>
        public const int DPSDK_EVENT_MOVE_ORG = 12;		// Move organization
        public const int DPSDK_EVENT_ADD_DEVICE = 13;		// Add device
        public const int DPSDK_EVENT_MODIFY_DEVICE = 14;		// Modify device
        public const int DPSDK_EVENT_DELETE_DEVICE = 15;		// Delete device
        public const int DPSDK_EVENT_MOVE_DEVICE = 16;		// Move device
        public const int DPSDK_EVENT_ALERT_USER = 17;		// User alert
        public const int DPSDK_EVENT_FACE_INFO = 31;        // Face recognition information
        public const int DPSDK_EVENT_MEDIA_SCREENSHOT = 103;		// Call back screenshot. The corresponding structure is MEDIA_DISPLAY
        public const int DPSDK_EVENT_DOWNLOAD_PROGRESS = 130;      // Download progress
        public const int DPSDK_EVENT_DOWNLOAD_CUT_FILE = 131;      // Split is finished
        public const int DPSDK_EVENT_DOWNLOAD_FILE_STARTPLAYBACK = 133;      // Start download by file
        public const int DPSDK_EVENT_DOWNLOAD_TIME_STARTPLAYBACK = 134;      // Start download by time
        public const int DPSDK_EVENT_VTCALL_INVITE = 431;       // Visual intercom call invitation notice
                                                                // Organization, device
        public const int DPSDK_DATA_ORG_INFO = 1;       // Organizational data A detailed view of the structure DPSDK_ORG_INFO
        public const int DPSDK_DATA_DEVICE_INFO = 2;        // Device data A detailed view of the structure DPSDK_DEV_ALL_INFO_LIST
        public const int DPSDK_DATA_COLLECT_ORG_INFO = 3;       // Collection tree A detailed view of the structure DPSDK_COLLECTION_ORG_INFO
        public const int DPSDK_DATA_DEVICE_LAYERED = 4;     // Hierarchical acquisition of device tree A detailed view of the structure DPSDK_LAYERED_RESULT_LIST
        public const int DPSDK_DATA_DEVICE_LIST_BY_ORG = 5;		// Device data A detailed view of the structure DPSDK_DEV_ALL_INFO_LIST
        public const int DPSDK_DATA_ALL_ORG_INFO = 6;		// All organizational data A detailed view of the structure 见DPSDK_ALL_ORG_INFO
        public const string XML_CODING = "coding";
        public const string XML_NAME = "name";
        public const string XML_SN = "sn";
        public const string DPSDK_DLL = "DPSDK.dll";
        public const int DPSDK_FILE_PATH_LEN = 1024;      // Length of file path          

        // Media Stream Callback Function
        public delegate IntPtr DPSDK_REALDATA_CALLBACK(IntPtr iMediaType, [MarshalAs(UnmanagedType.LPStr)] string pData, IntPtr iDataLen, IntPtr pUserParam);

        // Fish eye data callback
        public delegate void DPSDK_FISHEYE_CALLBACK(byte uszCorrectMode, UInt16 uRadius, UInt16 uCircleX, UInt16 uCircleY, uint uWidthRatio, uint uHeigthRatio, byte uszGain, byte uszDenoiseLevel, byte uszInstallStyle, IntPtr pUserParam);

        // Video plotting callback
        public delegate void DPSDK_DRAW_CALLBACK(IntPtr hDc, IntPtr pWnd, IntPtr pUserParam);

        // Data callback for the analysis of source data
        public delegate void DPSDK_DEMUXDEC_CALLBACK(IntPtr pUserParam, IntPtr iEncode);

        // Event callbacks
        public delegate void DPSDK_EVENT_CALLBACK(IntPtr iEventType, IntPtr iMediaSessionID, IntPtr pUserParam);

        // Replay the back wall callback
        public delegate IntPtr DPSDK_TVWALL_PLAYBACK_CALLBACK([MarshalAs(UnmanagedType.LPStr)] string pData, IntPtr iDataLen, IntPtr pUserParam);

        // Local Record Event Callback Funciton
        public delegate IntPtr DPSDK_EVENT_LOCALPLAY_CALLBACK(IntPtr iEventType, IntPtr iMediaSessionID, IntPtr pUserParam);

        /**
        * @brief	IVS Data Callback Funciton
        * @param	[IN] pData		Media Stream Data
        * @param	[IN] iDataLen	Data Length
        * @param	[IN] lRealLen	Real Length
        * @param	[IN] pReserved	Reserved Param
        * @param	[IN] pUserData		 User Data
        * @return	Returned value is 0 in case of success
        */
        public delegate IntPtr DPSDK_IVSDATA_CALLBACK([MarshalAs(UnmanagedType.LPStr)] string pData, IntPtr lType, IntPtr lDateLen, IntPtr lRealLen, IntPtr pReserved, IntPtr pUserData);

        // Record Event Callback Function
        public delegate void DPSDK_EVENT_DOWNLOAD_CALLBACK(IntPtr iEventType, IntPtr iMediaSessionID, IntPtr pData, IntPtr pUserParam);

        // Data sync callback. Used for upper level data copy.
        public delegate void DPSDK_DataCallback(IntPtr iDataType, IntPtr pDataBuf, IntPtr uiBufSize, IntPtr pUserData);

        // Event callback definition.
        public delegate void EventCallBack(IntPtr iEventType, IntPtr pEventBuf, IntPtr uiBufSize, IntPtr pUserData);

        // General begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_Init();

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_Uninit();

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_SetLogInfo([MarshalAs(UnmanagedType.LPStr)] string szLogPath, IntPtr iLogLevel);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_Login(ref DPSDK_LOGIN_PARAM pLoginParam, ref IntPtr pSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_Logout(IntPtr iSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetClientVersion(IntPtr iSessionID, ref byte pBuf, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetClientRoad(IntPtr iSessionID, ref byte pBuf, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetVersion();

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetEMapInfo(IntPtr iSessionID, ref DPSDK_SERVER_INFO pServerInfo);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SyncTime(IntPtr iSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetHttpsMode(IntPtr iHttpsMode);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ModifyUserPwd(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pNewPwd);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_SetEventCallBack(IntPtr iSessionID, EventCallBack fEventCallBack, IntPtr pUserData);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetFtpInfo(IntPtr iSessionID, uint uiDataType, IntPtr pFtpServerInfoList, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetMenuRight(IntPtr iSessionID, IntPtr pMenuRightList, uint uiBufLen, IntPtr pForbiddenMenuRightList, uint uiForbiddenLen);

        // Real preview begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartRealPlay(IntPtr iSessionID, ref DPSDK_REALPLAY_PARAM pRealPlayParam, ref IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StopRealPlay(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetPlayStreamMode(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pStreamMode);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetPlayStreamMode(IntPtr iSessionID, IntPtr pMediaSessionID, uint uiStreamMode, uint uiDelayTime);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetDisplayRegion(IntPtr iSessionID, IntPtr pMediaSessionID, ref DPSDK_RECT pRECT, IntPtr hDestWnd, bool bEnable);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_OperateRealPlay(IntPtr iSessionID, IntPtr pMediaSessionID, [MarshalAs(UnmanagedType.LPStr)] string pCodeID, int iOperateType);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetPictureSize(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pWidth, ref IntPtr pHeight);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_Get24BitPictureFile(IntPtr iSessionID, IntPtr pMediaSessionID, uint uiPicFormat, [MarshalAs(UnmanagedType.LPStr)] string pPath);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetPictureBuf(IntPtr iSessionID, IntPtr pMediaSessionID, ref byte pPicBuf, IntPtr iBufsize, ref IntPtr pPicSize, uint uiPicFormat);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_OpenSound(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_CloseSound(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_OpenSoundShare(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_CloseSoundShare(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_IsOpenSoundState(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pIsOpenSound);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetVolume(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pVolume);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetVolume(IntPtr iSessionID, IntPtr pMediaSessionID, uint uiVolume);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartRecord(IntPtr iSessionID, IntPtr pMediaSessionID, [MarshalAs(UnmanagedType.LPStr)] string pFile, uint uiSplitRecordLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StopRecord(IntPtr iSessionID, IntPtr pMediaSessionID, IntPtr pRecordFile, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_IsRecordState(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pIsRecord);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetSplitRecordLen(IntPtr iSessionID, IntPtr pMediaSessionID, uint uiSplitRecordLen);

        // PTZ function begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzOperateFunction(IntPtr iSessionID, ref DPSDK_PTZOPERATE_FUNCTION_PARAM pPtzOperateFunctionParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateFunctionResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzOperateCamera(IntPtr iSessionID, ref DPSDK_PTZOPERATE_CAMERA_PARAM pPtzOperateCamereParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzOperateDirect(IntPtr iSessionID, ref DPSDK_PTZOPERATE_DIRECT_PARAM pPtzOperateDirectParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzOperateFocus(IntPtr iSessionID, ref DPSDK_PTZOPERATE_FOCUS_PARAM pPtzOperateFocusParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzOperatePresetPoint(IntPtr iSessionID, ref DPSDK_PTZOPERATE_PRESETPOINT_PARAM pPtzOperatePrePointParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzGetPresetPoints(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pChannelId, IntPtr pPresetPointList, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzSitPosition(IntPtr iSessionID, ref DPSDK_PTZOPERATE_SITPOSITION_PARAM pPtzOperateSitPositionParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PtzArrangePtz(IntPtr iSessionID, ref DPSDK_PTZOPERATE_ARRANGEPTZ_PARAM pPtzOperateArrangePtzParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_AlarmActionOut(IntPtr iSessionID, ref DPSDK_PTZOPERATE_ALARMOUT_PARAM pAlarmOutParam, ref DPSDK_PTZOPERATE_RESULT pPtzOperateResult);

        // Alarm begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ConfirmAlarm(IntPtr iSessionID, IntPtr pConfirmAlarmParam);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_QueryAlarm(IntPtr iSessionID, ref DPSDK_QUERYALARM_PARAM pQueryAlarmParam, uint uiBufLen, IntPtr pAlarmDetailInfoList);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_GetAlarmTypeGroupInfo(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pLanguage, ref IntPtr pInfoXml);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_ReleaseDataBuffer(IntPtr pBuffer);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_QueryAlarmCount(IntPtr iSessionID, ref DPSDK_QUERYALARMCOUNT_PARAM pQueryAlarmCountParam, ref IntPtr pAlarmCount);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_QueryAlarmProcessFlow(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pAlarmCode, uint uiBufLen, IntPtr pAlarmProcessInfoList);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_BlockAlarm(IntPtr iSessionID, ref DPSDK_BLOCKALARM_PARAM pBlockAlarmParam);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ExportAlarms(IntPtr iSessionID, ref DPSDK_ALARMEXPORT_PARAM pAlarmExportParam, int iSessionId);

        // Playback begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_QueryRecord(IntPtr iSessionID, ref DPSDK_QUERY_RECORD_PARAM pQueryRecord, IntPtr pRecordList, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_QueryRecordDate(IntPtr iSessionID, ref DPSDK_QUERY_RECORD_DATE_PARAM pQueryDateInfo, ref DPSDK_RECORD_DATE_INFO pRecordDate);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetRecordStatus(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pChannelID, ref DPSDK_RECORD_STATUS_INFO pRecordInfo);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_LockRecordFile(IntPtr iSessionID, ref DPSDK_LOCK_RECORD_FILE_PARAM pLockFileInfo, ref DPSDK_LOCK_RECORD_FILE_RESULT pResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_UnlockRecordFile(IntPtr iSessionID, ref DPSDK_UNLOCK_RECORD_FILE_PARAM pUnlockFileInfo, ref DPSDK_LOCK_RECORD_FILE_RESULT pResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_QueryLockRecord(IntPtr iSessionID, ref DPSDK_QUERY_LOCK_RECORD_PARAM pQueryLockRecord, IntPtr pLockRecordList, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartRemoteRecord(IntPtr iSessionID, ref DPSDK_PTZOPERATE_STARTREMOTERECORD_PARAM pStartRemoteRecordParam, ref DPSDK_PTZOPERATE_REMOTERECORD_RESULT pStartRemoteRecordResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StopRemoteRecord(IntPtr iSessionID, ref DPSDK_PTZOPERATE_STOPREMOTERECORD_PARAM pStopRemoteRecordParam, ref DPSDK_PTZOPERATE_REMOTERECORD_RESULT pStopRemoteRecordResult);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StopPlayback(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartPlaybackByTime(IntPtr iSessionID, ref DPSDK_PLAYBACK_BY_TIME_PARAM pPlaybackParam, ref IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartPlaybackByFile(IntPtr iSessionID, ref DPSDK_PLAYBACK_BY_FILE_PARAM pPlaybackParam, ref IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PlaybackPause(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PlaybackResume(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PlaybackFrameStep(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PlaybackSeek(IntPtr iSessionID, IntPtr pMediaSessionID, ref DPSDK_PLAYBACK_SEEK_PARAM pPlaybackSeekParam);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetPlayedTime(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pTime);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetProviderType(IntPtr iSessionID, IntPtr pMediaSessionID, ref IntPtr pProviderType);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetPlaybackSpeed(IntPtr iSessionID, IntPtr pMediaSessionID, DPSDK_PLAYBACK_SPEED iSpeed);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartDownloadRecordByTime(IntPtr iSessionID, ref DPSDK_DOWNLOAD_BY_TIME_PARAM pDownloadByTimeParam, ref IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StartDownloadRecordByFile(IntPtr iSessionID, ref DPSDK_DOWNLOAD_BY_FILE_PARAM pDownloadByFileParam, ref IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_StopDownloadRecord(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_PauseDownloadRecord(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ResumeDownloadRecord(IntPtr iSessionID, IntPtr pMediaSessionID);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetDownloadRecordInfo(IntPtr iSessionID, IntPtr pMediaSessionID, IntPtr pDownloadInfo, uint uiBufLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ConvertToBmpFile(IntPtr iSessionID, IntPtr pMediaSessionID, ref DPSDK_CONVERT_BMP pConvertBMP);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_ConvertToJpegFile(IntPtr iSessionID, IntPtr pMediaSessionID, ref DPSDK_CONVERT_JPEG pConvertJPEG);

        //Organization begin
        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetDeviceByLayered(IntPtr iSessionID, ref DPSDK_GET_DEVICE_LAYERED_PARAM pParam, ref DPSDK_PAGE_INFO pPageInfo, ref IntPtr pTotal, DPSDK_DataCallback fDataCallBack, IntPtr pUserData);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetOrganization(IntPtr iSessionID, IntPtr pParam, uint uiLen, DPSDK_DataCallback fDataCallBack, IntPtr pUserData);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SaveUserData(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pUserDataName, [MarshalAs(UnmanagedType.LPStr)] string pUserData, uint uiDataLen);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_GetUserData(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pUserDataName, ref IntPtr pUserData);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_DelUserData(IntPtr iSessionID, [MarshalAs(UnmanagedType.LPStr)] string pUserDataName);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        private extern static DPSDK_RES DPSDK_SetCompressType(IntPtr iCompressType);

        [DllImport(DPSDK_DLL, CharSet = CharSet.Ansi)]
        public extern static DPSDK_RES DPSDK_GetDevice(IntPtr iSessionID, IntPtr pQueryDevInfo, uint uiQueryLen, DPSDK_DataCallback fDataCallBack, IntPtr pUserData);

        // Log rank The higher the level is, the less the content of the output is
        public enum DPSDK_LOG_LEVEL_TYPE
        {
            LOG_LEVEL_DEBUG = 2,		        // debugging Do not print normally for debugging and use
            LOG_LEVEL_INFO = 4,		        // information
            LOG_LEVEL_WARN = 5,		        // notice
            LOG_LEVEL_ERR = 6,		        // error
        }

        // Https model
        public enum DPSDK_HTTPS_MODE
        {
            DPSDK_HTTPS_UNENABLE = 0,               // Not enable https
            DPSDK_HTTPS_ONEWAY_AUTH = 1,                // Enable https one-way authentication
            DPSDK_HTTPS_TWOWAY_AUTH = 2					// Enable https two-way authentication
        }

        public enum DPSDK_FTP_DATA_TYPE
        {
            FTP_UNKNOW = 0,             // Unknown
            FTP_ALARM = 1,              // Alarm
            FTP_TAG = 2,                // Tag
            FTP_DOOR = 3	                // Acess
        }

        // Decoding type
        public enum DPSDK_DECODE_TYPE
        {
            DPSDK_DECODE_SW = 0,				// CPU decoding
            DPSDK_DECODE_HW = 1,				// GPU decoding
            DPSDK_DECODE_HW_FAST = 2					// GPU decoding
        }

        public enum DPSDK_VIDEO_LOCK_TYPE
        {
            DPSDK_VIDEO_CMD_LOCK = 0,				// Lock the current camera
            DPSDK_VIDEO_CMD_UNLOCK_ONE = 1				    // Unlock the current camera
        }

        // Capture image format
        public enum DPSDK_PIC_FORMAT
        {
            DPSDK_PIC_FORMAT_BMP = 0,               // BMP type
            DPSDK_PIC_FORMAT_JPEG = 1,              // JPEG type
            DPSDK_PIC_FORMAT_BMP24 = 6			        // BMP24 type
        }

        // Operating parameters of cloud platform
        public enum PtzOperateFunction_e
        {
            PtzOF_Show_PtzMenu = 0,             // Display "Cloud platform menu"
            PtzOF_Move_PtzMenu = 1,             // Control "Menu direction of the cloud platform"
            PtzOF_Confirm_PtzMenuItem = 2,              // Determine "Cloud platform menu item"
            PtzOF_Set_LineScannBorder = 3,              // Set up "Line scavenging boundary"
            PtzOF_Switch_LineScanBorder = 4,                // Switch "Line scan"
            PtzOF_Switch_AutoRotate = 5,                // Switch "Horizontal rotation"
            PtzOF_Switch_Light = 6,             // Switch "lighting"
            PtzOF_Switch_RainBrush = 7,             // Switch "Wiper"
            PtzOF_Switch_InfraredLight = 8,             // Switch "infrared light"
            PtzOF_Switch_AssisentPoint = 9,             // Switch "Auxiliary point"
            PtzOF_Switch_Cruise = 10,               // Switch "Cruise function"
            PtzOF_Switch_Track = 11,                // Switch "Cruising"
            PtzOF_Switch_SetTrack = 12              // Switch "Track setting"
        }

        // Video source
        public enum DPSDK_SOURCE_TYPE
        {
            DPSDK_SOURCE_TYPE_ALL = 1,                //All video, including platform video and device video
            DPSDK_SOURCE_TYPE_DEVICE = 2,                //Device video
            DPSDK_SOURCE_TYPE_CENTER = 3,				//Platform video
            DPSDK_SOURCE_TYPE_3RD_CLOUD = 4,                //3rd platform video
        }

        // Stream type
        public enum DPSDK_STREAM_TYPE
        {
            STREAM_UNKNOW_STREAM = 0,                // Unknown
            STREAM_MAIN_STREAM = 1,                // Main stream
            STREAM_SUB_STREAM = 2,                // Auxiliary code stream
            STREAM_THIRD_STREAM = 3,                // Three bit stream
            STREAM_LOCAL_SIGNAL_STREAM = 5	                // Local signal
        }

        // Record type
        public enum DPSDK_RECORD_TYPE
        {
            DPSDK_RECORD_TYPE_ALL = 0,
            DPSDK_RECORD_TYPE_MANUAL = 1,                // Manual record
            DPSDK_RECORD_TYPE_ALARM = 2,                // Alarm record
            DPSDK_RECORD_TYPE_MOTION_DETECT = 3,                // Dynamic detection
            DPSDK_RECORD_TYPE_VIDEO_LOST = 4,               // Video loss
            DPSDK_RECORD_TYPE_VIDEO_SHELTER = 5,                // Video occlusion
            DPSDK_RECORD_TYPE_TIMER = 6,                // Timing video
            DPSDK_RECORD_TYPE_ALLDAY = 7,                // All-weather video
            DPSDK_RECORD_TYPE_FILE_RECORD = 8,              // File video conversion
            DPSDK_RECORD_TYPE_NORMAL = 9,                // Ordinary video

            DPSDK_RECORD_TYPE_CARD = 25,               // Card number video There is no this in the protocol library for the time being
            DPSDK_RECORD_TYPE_ALARM_BEGIN = 10,             // Alarm start Definition in the match protocol stack 10~300 -m -f -cSpecial alarm
            DPSDK_RECORD_TYPE_ALARM_END = 1000				// End of intelligent alarm Definition in the match protocol stack 300~1000Intelligent alarm
        }

        // Playback speed
        public enum DPSDK_PLAYBACK_SPEED
        {
            DPSDK_PB_NORMAL = 1024,
            DPSDK_PB_NORMAL_FAST2 = DPSDK_PB_NORMAL * 2,
            DPSDK_PB_NORMAL_FAST4 = DPSDK_PB_NORMAL * 4,
            DPSDK_PB_NORMAL_FAST8 = DPSDK_PB_NORMAL * 8,
            DPSDK_PB_NORMAL_FAST16 = DPSDK_PB_NORMAL * 16,
            DPSDK_PB_NORMAL_SLOW2 = DPSDK_PB_NORMAL / 2,
            DPSDK_PB_NORMAL_SLOW4 = DPSDK_PB_NORMAL / 4,
            DPSDK_PB_NORMAL_SLOW8 = DPSDK_PB_NORMAL / 8,
            DPSDK_PB_NORMAL_SLOW16 = DPSDK_PB_NORMAL / 16
        }

        public enum DPSDK_RECORD_FILE_NAME_RULE
        {
            DPSDK_NAME_RULE_TIME_CHANNELID = 0,
            DPSDK_NAME_RULE_TIME_CHANNELNAME = 1,
            DPSDK_NAME_RULE_CHANNELID_TIME = 2,
            DPSDK_NAME_RULE_CHANNELNAME_TIME = 3
        }

        public enum DPSDK_DOWNLOAD_RECORD_FILE_FORMAT
        {
            DPSDK_FILE_FORMAT_NORMAL = 0,               // Original stream
            DPSDK_FILE_FORMAT_AVI = 1,              // avi format
            DPSDK_FILE_FORMAT_MP4 = 2,              // mp4 format
            DPSDK_FILE_FORMAT_FLV = 3,              // flv format
            DPSDK_FILE_FORMAT_ASF = 4				    // asf format
        }

        // Compression method
        public enum DPSDK_COMPRESS_TYPE
        {
            COMPRESS_DISABLE = 0,               // No use of compression
            COMPRESS_DEFAULT = 1				    // Using the default compression method
        }

        // Login information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LOGIN_PARAM
        {
            public byte bDomainUser;			// Whether or not domain login
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szUserName;             // User name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szPWD;                  // Cleartext password, the login type is0(basic account), can not be empty
            public DPSDK_IP struIP;                 // Login server IP
            public uint uiPort;                 // Login server port
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szMACAddress;           // AC address
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szIMEI;                 // Check code for mobile client landing platform
            public uint uiClientType;           // Client type: Reference DPSDK_CLIENT_TYPE
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szReserve;              // Reserved field
            public DPSDK_IP struClientIP;           // Client IP
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_IP
        {
            public int uiIPType;               // IP type, refer to DPSDK_IP_TYPE
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szIP;                   // IP address
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_SERVER_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szIP;                   // server IP
            public uint uiPort;                 // server port
        }

        // FTP server list information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FTP_SERVER_INFO_LIST
        {
            public uint uiTotal;                // The total number of result
            public IntPtr struFtpServerInfo;		// FTP server information
        }

        // FTP server information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FTP_SERVER_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szUrl;                  // FTP absolute path ftps://192.168.1.1:21
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szUserName;             // User name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szPWD;                  // Password
            public uint uiDataType;             // Data type Refer: DPSDK_FTP_DATA_TYPE
        }

        // The list of menu right
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MENU_RIGHT_LIST
        {
            public uint uiTotal;                // The total number of result
            public IntPtr struMenuRight;			// The list of menu right
        }

        // Menu information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MENU_RIGHT
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szMenuRight;            // Menu right information
        }

        // Device status change notification
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEV_STATUS_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceID;             // Device ID
            public int iStatus;				// Device status see DPSDK_DEV_STATUS
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szOfflineReason;        // Off-line reason
        }

        // Channel state change notification
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CHANNEL_STATUS_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelID;            // ChannelID
            public int iStatus;				// Channel status see DPSDK_DEV_STATUS
        }

        // Organization of basic data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ORG_BASE_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;              // organization Code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szOrgName;              // Organization name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szOrgSN;                // organization SN code
            public int iOrgType;                // Organization node type
            public int iOrgSort;                // Organization sort
            public Int64 tModifyTime;           // Modify time

            //
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szParentCode;           // Organization parent node ID

            //Cloud SDK parameter
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szParentName;           // Organization parent node name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szPlatformCode;         // GB ID
            public int iGroupNumber;            // Number of department under this node
            public int iDevivceNumber;          // Number of device under this node
            public int iChannelNum;			// Number of channel under this node
        }

        // Mobile organization notification
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MOVE_ORG_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOldOrgCode;           // Old organization node Code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOldParentOrgCode;     // Old organization father node Code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szNewOrgCode;           // New organization nodeCode
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szNewParentOrgCode;     // New organization father node Code
        }

        // Increase the device notification
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ADD_DEVICE_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;              // Organization code
            public DPSDK_DEV_ALL_INFO struDevAllInfo;			// Device data
        }

        // Modification of device notification
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MODIFY_DEVICE_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOldOrgCode;           // Old organization
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szNewOrgCode;           // New organization
            public DPSDK_DEV_ALL_INFO struDevAllInfo;			// Device data
        }

        // Delete device notification Support batch
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DELETE_DEVICE_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;              // organization code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceID;             // Device ID
        }

        // Mobile device notification Support batch
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MOVE_DEVICE_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceID;             // Device ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOldOrgCode;           // The original organization of the equipment code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szNewOrgCode;           // New organization of equipment code
        }

        // Unicast video parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_REALPLAY_PARAM
        {
            public DPSDK_MEDIA_BASE_PARAM struMediaBaseParam;   // Basic video parameters
            public DPSDK_MEDIA_CALLBACK struMediaCallBack;      // Video callback structure

            //Transcoding parameter
            public int iUsedVcs;               // Whether the tag needs to pass throughVCSTranscoding.0It means that there is no need for transcoding;1It means that transcoding is required
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szVideoCode;	        // Video coding format, reference video coding format to define strings
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szResolution;           // Code stream resolution, reference stream resolution definition string
            public int iFps;                   // Frame rate
            public int iBps;                   // Bit stream code stream
        }

        // Basic video parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MEDIA_BASE_PARAM
        {
            public IntPtr pHWnd;                  // Window handle
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szCodeId;               // Channel ID Or equipment ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceCode;           // Device code for request media key
            public int iStreamType;            // Code stream type 1=Main stream, 2=Auxiliary code stream
            public int iDataType;              // Video type:1=video, 2=audio frequency, 3=Audio and video
            public int iDecodeType;            // Decode type See DPSDK_DECODE_TYPE Definition
            public int iStreamMode;            // Playback mode See DPSDK_STREAM_MODE Definition
            public uint uiDelayTime;            // Play delay time, when IstreamMode is  DPSDK_STREAM_CUSTOM_MODETime, it is  effective Company MS
        }

        // DPSDK_MEDIA_CALLBACK
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_MEDIA_CALLBACK
        {
            public DPSDK_REALDATA_CALLBACK fRealDataCallBack;   // Bitstream callback
            public IntPtr pRealUserData;          // Code stream callback user data

            public DPSDK_FISHEYE_CALLBACK fFishEyeCallBack;     // Fish eye data callback
            public IntPtr pFishEyeUserData;       // Fish eye data callback user data

            public DPSDK_DRAW_CALLBACK fDrawCallBack;          // Video plotting callback
            public IntPtr pDrawUserData;          // Video plotting callback user data

            public DPSDK_DEMUXDEC_CALLBACK fDemuxDecCallBack;   // Data callback for the analysis of source data
            public IntPtr pDemuxDecUserData;      // Data callback to user data analyzed by source data

            public DPSDK_EVENT_CALLBACK fEventCallBack;         // Event callbacks
            public IntPtr pEventUserData;         // Event callback user data

            public DPSDK_TVWALL_PLAYBACK_CALLBACK fTVWallPlaybackCallBack; //Replay the back wall callback
            public IntPtr pTVWallPlaybackUserData;// Playback the upper wall callback user data

            public DPSDK_EVENT_LOCALPLAY_CALLBACK fEventLocalCallBack;// Local Record Event Callback
            public IntPtr pEventLocalUserData;    // Local Record Event Callback user data

            public DPSDK_IVSDATA_CALLBACK fIVSDataCallBack;
            public IntPtr pIVSUserData;		    // Local Record Event Callback user data
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_RECT
        {
            public Int64 left;
            public Int64 top;
            public Int64 right;
            public Int64 bottom;
        }

        // Video file list
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FILE_STORE_LIST
        {
            public UInt64 uiTotal;                // Total number of video files
            public IntPtr pFileList;              // Video file list
        }

        // Video file path
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FILE_STORE_INFO
        {
            public uint uiStoreLen;             // Video length
            public Int64 lBeginTime;		        // Video start time
            public Int64 lEndTime;		        // Video end time
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szFile;	                // Full path of video files
        }

        // Operating parameters of cloud platform
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_FUNCTION_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public PtzOperateFunction_e iPtzOFType;             // Operating function type of cloud platform
            public int iCruiseId;              // (This variable is only in Switch_CruiseOperation when effective) Cruise ID
            public int iTrackId;               // (This variable is only in Switch_Track、Switch_SetTrackEffective operation during operation ID
            public int iSwitchMode;            // (This variable is only in SwitchEffective operation)0-Close，1-open
            public int iBorderType;            // (This variable is only in Set_LineScannBorderEffective operation)16-Left boundary17-.Right boundary
            public int iAssisentType;          // (This variable is only in Switch_AssisentPointEffective operation)23-Backlight compensation,24-Number doubled,27- Color turn black,35-Shutter time,41-Brightness,42-Image flip,43-The name of the preset point is hidden,80-Restore factory settings
            public int iMoveType;              // (This variable is only in Move_PtzMenuEffective operation)25-Upward movement,26-Move down,27-Left shift,28-Right
            public int iSwitchPtzMenu;         // (This variable is only in Show_PtzMenuEffective operation)22=Open the platform menu,23=Close the cloud table menu
        }

        // Function operation result of cloud platform 
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_RESULT
        {
            public DPSDK_PTZ_LOCKUSER struLockUser;
            public int iResult;				// Operation results:0-failure,1-success
        };

        // Lock holder information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZ_LOCKUSER
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szLockUserName;         // User name for lockin cloud
            public int iLockUserLevel;         // Lock user level of cloud
        }

        // Operating cloud platform camera parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_CAMERA_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public int iDirect;                // Direction:1-increase,2-decrease
            public int iCommand;               // Order:0-stop it，1-open
            public int iStep;                  // Step 
            public int iOperateType;           // Operation type:1-variable,2-zoom,3-aperture
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szExtend;		        // Extended data
            public double fSpeed;                 // The speed of PTZ,normalized 0~1，0 represent default speed, used when vsl zoomed
            public int iDuration;              // Duration,unit:ms
        }

        // Cloud platform direction control parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_DIRECT_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public int iStepY;                 // Vertical direction step
            public int iStepX;                 // Horizontal direction step
            public int iDirect;                // Direction:1-On,2-Under the,3-Left,4-Right,5-Upper left,6-Lower left,7-On the right,8-lower right
            public int iCommand;               // Order:0-Stop it, 1-open
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szExtend;               // Extended data
            public int iDuration;              // The duration of PTZ turned, unit:ms
        }

        // Electric focusing control parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_FOCUS_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public float fFocus;                 // Focal length 
            public float fZoom;                  // Multiple
            public int iOperateType;           // Operation type:0-Reset,1-Continuous focusing,2-Autofocus
        }

        // Control preset point parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_PRESETPOINT_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szPointCode;	        // Preset point coding
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szPointName;          // Preset point name
            public int iOperateType;               // Operation type:1-Location, 2-Set up, 3-delete, 4-Update working time
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szStartTime;			// Start time(time stamp)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szEndTime;                // End time(time stamp)
            public double fSpeed;                 // The speed of PTZ turning, normalized 0~1，0 represent default speed
        }

        // Get a list of preset points
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZ_PRESETPOINT_LIST
        {
            public uint uiTotal;                // Total
            public IntPtr struPresetPointInfo;    // Preset point list
        }

        // Get the preset point information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZ_PRESETPOINT_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szPointName;	        // Preset point name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szPointCode;          // Preset point encoding, from 1 start
            public int iPointType;             // Preset point type,0=Ordinary preset point,1=Preset points that have been set for intelligent rules
        }

        // Three dimensional positioning parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_SITPOSITION_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public double fPointX;                // Horizontal coordinates:-8192 - Eight thousand one hundred and ninety-two
            public double fPointY;                // Vertical coordinates:-8192 - Eight thousand one hundred and ninety-two
            public double fPointZ;                // Variable number:-4 - 4
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szExtend;               // Extended data
        }

        // Lock the unlocking parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_ARRANGEPTZ_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public uint uiLockTime;             // Lock time, unit second,texpression has been locked until the release or Bei Qiang Wins 
            public int iOperateType;           // Operation type:0-Unknown, 1-Lock the current camera, 2-Unlock the current camera, 3-Unlock all the cameras locked by the user, 4-Lock all the cameras, 5-Query lock state
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szExtend;               // Extended data
        }

        // Alarm output control parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_ALARMOUT_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            public int iOperateType;           // Control type:1-Status control,2-Pattern control
            public int iCommand;               // Control commands: state control,1-Open,0-Shut down; mode control:0-Close，1-Automatically,2-Manual
        }

        // Alarm confirmation parameter
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CONFIRMALARM_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Handling human username
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 766)]
            public string szHandleMessage;        // Handling opinions
            public uint uiEmailRevceiverNumber; // Alarm processing mailbox number
            public int iHandleStatus;          // Processing state (Reference resources AlarmDealWith_e)
            public IntPtr struEmailReceiverList;  // Alarm processing notification mailbox list
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string szAlarmComment;
        }

        // E-mail address
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_EMAILADDRESS
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 320)]
            public string szEmailAddr;            // E-mail address
        }

        // Alarm query parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERYALARM_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szBeginTime;            // Start time of alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szEndTime;              // The end time of the alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleBeginTime;      // Alarm processing start time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleEndTime;        // Alarm processing end time yyyymmddhhmmss
            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            //public string               szDeviceId;             // Device ID
            public IntPtr pDeviceIdList;
            public int iDeviceIdNum;
            public IntPtr pChannelIdList;
            public int iChannelIdNum;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szOrgId;                // Organization node ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szAlarmId;              // Alarm ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code (specifying this condition to ignore other conditions）
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Alarm processing person
            public int iPageSize;              // Number of alerts per page
            public int iPageNo;                // Query page number (from 1Start）
            public int iSortType;              // Sort fields (1=Alarm time,2=Alarm type,3=Alarm level,4=report Police officer,5=Processing state）
            public int iSortOrder;             // Sort direction (0=Ascending order,1=Descending order）
            public IntPtr pAlarmType;             // Alarm type (Reference resources Alarm_type_e)
            public uint uiAlarmTypeNumber;      // Number of alarm types
            public IntPtr pAlarmGrade;            // Alarm level (Reference resources AlarmLevel_e)
            public uint uiAlarmGradeNumber;     // The number of alarm levels
            public IntPtr pAlarmStatus;           // Alarm state (Reference resources AlarmState_e)
            public uint uiAlarmStatusNumber;    // The number of alarm states
            public IntPtr pHandleStatus;          // Alarm processing state (Reference resources AlarmDealWith_e)
            public uint uiHandleStatusNumber;   // The number of state of the alarm processing
        }

        // Channel code
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CHANNEL_ID
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szId;                   // Channel ID
        };

        // Alarm record list
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARM_DETAILINFO_LIST
        {
            public Int64 tQueryTime;                // The service time of this query,unit seconds
            public uint uiTotal;                // Total number of alarm records
            public IntPtr struAlarmInfoList;      // Alarm record
        }

        // Alarm information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARM_DETAILINFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szAlarmId;              // Alarm ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceId;             // Device ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szDeviceName;           // Device name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szChannelName;          // Channel name
            public int iAlarmGrade;            // Alarm level(Reference resources AlarmLevel_e)
            public int iAlarmType;             // Alarm type(Reference resources Alarm_type_e)
            public int iAlarmStatus;           // Alarm state(Reference resources AlarmState_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Alarm processing person
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleTime;           // Alarm processing time yyyymmddhhmmss
            public int iHandleStatus;          // Alarm processing state(Reference resources AlarmDealWith_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 766)]
            public string szHandleMessage;        // Handling opinions
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szAlarmTime;            // Alarm time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256 * 16)]
            public string szAlarmPicture;         // Alarm snapshot path
            public uint uiAlarmPictureSize;     // Alarm snapshot size
            public uint uiEmailReceiverListSize;// The actual number of notification mailbox lists is not greater than that of the alarm DPSDK_EMAILRECEIVERLIST_SIZE
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public DPSDK_EMAILADDRESS[] struEmailReceiverList;  // Alarm processing notification mailbox list(Most return DPSDK_EMAILRECEIVERLIST_SIZEA mail address)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 200)]
            public DPSDK_ALARM_MEMO_INFO[] struMemoList;        // Alarm memo list
            public int iMemoNum;               // Alarm memo number
            public Int64 tQueryTime;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARM_MEMO_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szMemo;                 // Alarm memo data
        }

        // Alarm total query parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERYALARMCOUNT_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szBeginTime;            // Start time of alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szEndTime;              // The end time of the alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleBeginTime;      // Alarm processing start time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleEndTime;        // Alarm processing end time yyyymmddhhmmss
            //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            //public string               szDeviceId;             // Device ID
            public IntPtr pDeviceIdList;
            public int iDeviceIdNum;
            public IntPtr pChannelIdList;
            public int iChannelIdNum;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szOrgId;                // Organization node ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szAlarmId;              // Alarm ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code (specifying this condition to ignore other conditions)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Alarm processing person
            public IntPtr pAlarmType;             // Alarm type (Reference resources Alarm_type_e)
            public uint uiAlarmTypeNumber;      // Number of alarm types
            public IntPtr pAlarmGrade;            // Alarm level (Reference resources AlarmLevel_e)
            public uint uiAlarmGradeNumber;     // The number of alarm levels
            public IntPtr pAlarmStatus;           // Alarm state (Reference resources AlarmState_e)
            public uint uiAlarmStatusNumber;    // The number of alarm states
            public IntPtr pHandleStatus;          // Alarm processing state (Reference resources AlarmDealWith_e)
            public uint uiHandleStatusNumber;   // The number of state of the alarm processing
        }

        // Alarm processing record list
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMPROCESS_DETAILINFO_LIST
        {
            public uint uiTotal;                // Total number of alarm processing information
            public IntPtr struAlarmProcessInfoList;// Alarm processing information
        }

        // Alarm processing record
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMPROCESS_DETAILINFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Alarm processing person
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleTime;           // Alarm processing time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 766)]
            public string szHandleMessage;        // Warning handling opinion
            public int iHandleStatus;          // Alarm processing state(Reference resources AlarmDealWith_e)
        }

        // Shielded alarm parameter
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_BLOCKALARM_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
            public string szAlarmCodeSource;      // Shielded alarm source (device alarm for device code, channel alarm as channel Code, system alarm for service code)
            public int iAlarmType;             // Shielding alarm type (Reference resources Alarm_type_e)
            public int iDuration;              // The length of the shielding time (unit: Second)
        }

        // Alarm export parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMEXPORT_PARAM
        {
            public int iSortType;              // Sort fields (1=Alarm time,2=Alarm type,3=Alarm level,4=AlarmTake care of people,5=Processing state)
            public int iSortOrder;             // Sort direction (0=Ascending order,1=Descending order)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szAlarmId;              // Alarm ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szOrgId;                // Organization node ID
                                                  //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
                                                  //public string               szDeviceId;             // Device ID
            public IntPtr pDeviceIdList;
            public int iDeviceIdNum;
            public IntPtr pChannelIdList;
            public int iChannelIdNum;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szBeginTime;            // Start time of alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szEndTime;              // The end time of the alarm yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleBeginTime;      // Alarm processing start time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szHandleEndTime;        // Alarm processing end time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Alarm processing person
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szLanguage;             // Language
            public IntPtr pAlarmType;             // Alarm type(Reference resources Alarm_type_e)
            public uint uiAlarmTypeNumber;      // Number of alarm types
            public IntPtr pAlarmGrade;            // Alarm level(Reference resources AlarmLevel_e)
            public uint uiAlarmGradeNumber;     // The number of alarm levels
            public IntPtr pAlarmStatus;           // Alarm state(Reference resources AlarmState_e)
            public uint uiAlarmStatusNumber;    // The number of alarm states
            public IntPtr pHandleStatus;          // Alarm processing state(Reference resources AlarmDealWith_e)
            public uint uiHandleStatusNumber;   // The number of state of the alarm processing
        }

        // Alarm event (notice)
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMEVENT_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;			// Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 90)]
            public string szAlarmNodeCode;        // Alarm source code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szAlarmTime;            // Alarm time yyyymmddhhmmss
            public int iAlarmGrade;         // Alarm level (Reference resources AlarmLevel_e)
            public int iAlarmStatus;            // Alarm state (Reference resources AlarmState_e)
            public int iAlarmObjType;           // Alarm object type (Reference resources AlarmObject_e)
            public int iAlarmType;              // Alarm type (Reference resources Alarm_type_e)
            public int iAlarmCategory;			// Type of alarm (Reference resources AlarmCategory_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szAlarmMessage;         // Alarm extension information (for example,GPSThe extended information of the alarm includes the latitude and longitude, the heightEtc
            public uint uiAlarmLinkVedioListSize;// Alarm video linkage information list number(Not greater than DPSDK_ALARM_LINKVEDIOINFOLIST_SIZE)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public DPSDK_ALARMLINKVEDIO_INFO[] struAlarmLinkVedioList;// Alarm video linkage information list(Most returnDPSDK_ALARM_LINKVEDIOINFOLIST_SIZEVideo linkage information)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szAlarmPicture;         // Alarm smart snapshot path
            public uint uiAlarmPictureSize;     // Alarm smart snapshot size
            public int iScreenNum;				// The number of Screen
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 200)]
            public DPSDK_ALARM_MEMO_INFO[] struMemoList;        // Alarm memo list
            public int iMemoNum;               // Alarm memo number
        }

        //Face recognition information - ryan
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FACE_INFO_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelID;      // Channel ID
            public int IFaceImageId;        // Snap face ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string SzFaceImageUrl;   // Take a picture of a face
            public bool BHited;             // Is it a hit
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string SzPictureUrl;     // Scene graph
            public int IRecAge;             // Identification age
            public int IRecExpress;         // Distinguish Express
            public int IRecFringe;          // Identify the Liu Hai 0-no 1-yes
            public int IRecSex;             // Identification of sex 0-Unknown 1-male 2-female
            public int IRecGlasses;         // Eyeglasses 0-no 1-glasses 2-Sunglasses
            public int IRecEmotion;         // Recognition of emotions 0-Smile 1-anger 2-Sadness 3-Hate 4-Fear 5-surprised 6-normal 7-Laugh
            public int IAppearTimes;        // Number of historical occurrences
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string SzBeginTime;       // View time
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string SzEndTime;         // Departure time
            public uint UiSimilarFaceListSize;              // Number of similar face information
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
            public DPSDK_FACE_INFO[] StruSimilarFaceList;   // Similar face information list
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_FACE_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string SzFaceImageUrl;   // Face map
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string SzName;           // Full name
            public int IGender;             // Gender 0-Unknown 1-male 2-female
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string SzBirthday;       // Birthday
            public int IPersonType;         // Type of personnel
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string SzPersonId;       // personnel Id
            public float FSimilarity;       // Similarity degree
            public bool BSurveillance;      // Whether dispatched
        }

        // Alarm linkage video information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMLINKVEDIO_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szLinkVedioId;          // Linkage video channel ID
            public int iStreamType;         // Code stream type
            public int iScreenId;				// Screen ID
        }

        // Alarm confirmation (notice)
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMCONFIRM_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szAlarmTime;            // Alarm time yyyymmddhhmmss
            public int iHandleStatus;			// Processing state
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 766)]
            public string szHandleMessage;        // Handling opinions
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHandleUser;           // Handling human user name
            public uint uiEmailReceiverListSize;// The actual number of notification mailbox lists is not greater than that of the alarm. DPSDK_EMAILRECEIVERLIST_SIZE）
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public DPSDK_EMAILADDRESS[] struEmailReceiverList;  // Alarm processing notification mailbox list(Most return PSDK_EMAILRECEIVERLIST_SIZEA mail address)
            public int iAlarmGrade;			// Alarm level (Reference resources AlarmLevel_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szAlarmPicture;         // Alarm smart snapshot path
            public uint uiAlarmPictureSize;     // Alarm smart snapshot size
            public int iAlarmStatus;            // Alarm state (Reference resources AlarmState_e)
            public int iAlarmType;				// Alarm type (Reference resources Alarm_type_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceId;             // Device ID
            public int iChannelSeq;         // Channel number
            public int iUnitType;               // Unit type
            public int iAlarmObjType;			// Alarm object type (Reference resources AlarmObject_e)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 90)]
            public string szAlarmNodeCode;        // Alarm source code
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 200)]
            public DPSDK_ALARM_MEMO_INFO[] struMemoList;        // Alarm memo list
            public int iMemoNum;				// Alarm memo number
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
            public string szAlarmComment;
        }

        // Alarm information (notice)
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARM_DETAILINFO_NOTIFY
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szAlarmCode;            // Alarm code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szAlarmTime;            // Alarm time yyyymmddhhmmss
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256 * 16)]
            public string szAlarmPicture;         // Alarm snapshot path
            public uint uiAlarmPictureSize;		// Alarm snapshot size
        }

        // Alarm export results (notice)
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMEXPORT_RESULT_NOTIFY
        {
            public uint uiSessionId;			// Session marking
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szDownloadPath;         // Downloading path
        }

        // Query record information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERY_RECORD_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;             // Channel ID
            public DPSDK_STREAM_TYPE iStreamType;            // Code stream type
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source type
            public DPSDK_RECORD_TYPE iRecordType;            // Video type
            public Int64 tBeginTime;             // Start time
            public Int64 tEndTime;               // End time
        }

        // Record information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_RECORD_INFO_LIST
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;             // Channel ID
            public uint iRetCount;              // Return the number of records, that is record number of recorded video records
            public IntPtr struSingleRecord;       // Video recording information
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_SINGLE_RECORD_INFO
        {
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source
            public DPSDK_RECORD_TYPE iRecordType;            // Video type. See RecordType_e
            public Int64 iStartTime;             // Start time
            public Int64 iEndTime;               // End time
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szName;                 // The name of the video (different manufacturers are different in the identification of the documents)			
            public Int64 iLength;                // File length, unit KB
            public DPSDK_STREAM_TYPE iStreamType;            // Code stream type

            // Here's the information needed for the center video
            public Int64 iPlanId;                // Video plan ID
            public int iSSId;                  // Storage service ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szDiskId;               // Disk ID
            public int iFileHandle;            // File handle
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelCode;          // Channel coding
            public byte bRecordHidden;          // Video hiding state True: concealment ；False Visible
            public byte bForgotten;             // Do you forget to forget the video

            // The informations of alarm video what are add
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szAlarmChannelId;       // Video camera ID
            public byte bLocked;                // Whether or not to be locked,Device video will not be locked
        }

        // Query record data parameter
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERY_RECORD_DATE_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;             // Channel ID
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source type
            public int iYear;                  // Year
            public int iMonth;                 // Month
        }

        // Record data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_RECORD_DATE_INFO
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 31)]
            public int[] RecordDays;             // The record is video taped. 0 start for the first day
        }

        // Channel video information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_RECORD_STATUS_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelCode;          // Channel coding	
            public int iChannelSeq;            // Channel number
            public int iRecordStatus;          // Video status SeeDPSDK_RECORD_STATUSDefinition
            public int iFlow;                  // Average flow rate (Kbps）
            public int iStreamType;            // Code stream type See DPSDK_STREAM_TYPEDefinition
            public int iUsedCapacity;          // Used storage capacity
        }

        // Lock record file
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LOCK_RECORD_FILE_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;			    // Camera ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFilename;             // The name of the video (different manufacturers are different in the identification of the documents)
        }

        // Locking or unlocking the results of video files
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LOCK_RECORD_FILE_RESULT
        {
            public int iLockNum;               // Lock number
        }

        // Unlock record file
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_UNLOCK_RECORD_FILE_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;			    // Camera ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFilename;             // The name of the video (different manufacturers are different in the identification of the documents)
            public byte bForce;                 // Whether or not compulsory
        }

        // Query record lock information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERY_LOCK_RECORD_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szUserId;             // User ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;               // Camera ID
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source, platform video or device video
            public Int64 tStartTime;             // The start time of lock
            public Int64 tEndTime;               // The unlock time of lock
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szReason;			    // The reason of lock
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_RECORD_LOCK_INFO_LIST
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraID;               // Camera ID
            public int iRetCount;              // The number of returned
            public IntPtr struSingleRecordLockInfo;// Used to save the informations of video lock which are query, the size of it depend on iReqCount
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_SINGLE_RECORD_LOCK_INFO
        {
            public int iLockId;                // Video lock ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szUserId;             // Lock user
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source, platform video or device video
            public Int64 tOperateTime;           // Operate time
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCameraId;			    // Camera ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szUserIp;			    // User IP
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szUserName;               // User name
            public Int64 tStartTime;             // The start time of lock
            public Int64 tEndTime;               // The end time of lock
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szReason;			    // The reason of lock
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFileName;			    // The name of record file
        }

        // Open the manual video parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_STARTREMOTERECORD_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Camera ID
            public int iStreamType;            // Code stream type (code stream type:1-Main stream, 2-Auxiliary code stream)
            public int iRecordDuration;        // Video length(default 12*3600s)
        }

        // Open/Stop the result of manual video
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_REMOTERECORD_RESULT
        {
            public int iPlanId;                // Plan ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
            public string szNow;                  // Current time(time stamp)
        }

        // Turn off the manual video parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PTZOPERATE_STOPREMOTERECORD_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Camera ID
            public int iStreamType;            // Code stream type (code stream type:1-Main stream, 2-Auxiliary code stream)
            public byte bForce;                 // Is it forced to close
        }

        // Playback by time parameter
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PLAYBACK_BY_TIME_PARAM
        {
            public DPSDK_MEDIA_CALLBACK struMediaCallBack;      // Video callback function

            public IntPtr pHWnd;                  // Window handle
            public int iDirection;             // Playback direction See DPSDK_PLAY_DIRECTION Definition

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCodeId;               // Channel id or device id
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceCode;           // Device code for request media key
            public Int64 tBeginTime;             // Start time(time stamp)
            public Int64 tPlayTime;              // Start playing time
            public Int64 tEndTime;               // End time(time stamp)
            public int iRecordSource;          // Video source, see see DPSDK_SOURCE_TYPE
            public int iStreamType;            // Code stream type, see see DPSDK_STREAM_TYPE
            public int iRecordType;            // Video type, see see DPSDK_RECORD_TYPE
        }

        // Playback by file parameter
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PLAYBACK_BY_FILE_PARAM
        {
            public DPSDK_MEDIA_CALLBACK struMediaCallBack;      // Video callback function

            public IntPtr pHWnd;                  // Window handle
            public int iDirection;             // Playback direction See DPSDK_PLAY_DIRECTION Definition

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szCodeId;               // Channel ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceCode;           // Device code for request media key
            public Int64 tBeginTime;             // Start time
            public Int64 tEndTime;               // End time
            public int iRecordSource;          // Video source, see DPSDK_SOURCE_TYPE
            public UInt64 uSSId;                  // Storage service (IDReturn to the query)
            public UInt64 uFileHandle;            // File handle(Return to the query)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szDiskId;               // Disk ID(Return to the query)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFilename;             // The name of the video (different manufacturers are different in the identification of the documents)
        }

        // Seek playback
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PLAYBACK_SEEK_PARAM
        {
            public Int64 tBeginTime;             // Start time
            public Int64 tEndTime;               // End time
            public DPSDK_PLAYBACK_SPEED iSpeed;                 // Playback speed
            public int iDirection;             // Playback direction See DPSDK_PLAY_DIRECTION Definition
            public int iNotCheckIFrameTimeOffset;// Check IFrameTimeOffset, 0 check, 1 not check
        }

        // Download parameters by time
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DOWNLOAD_BY_TIME_PARAM
        {
            public DPSDK_EVENT_DOWNLOAD_CALLBACK fEventCallBack;// Event callbacks
            public IntPtr pEventUserData;         // Event callback user data

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceCode;           // Device code for request media key
            public DPSDK_SOURCE_TYPE iSourceType;           // Video source
            public DPSDK_STREAM_TYPE iStreamType;           // Code stream type
            public DPSDK_RECORD_TYPE iRecordType;           // Video type
            public Int64 tBeginTime;                // start time
            public Int64 tEndTime;				// End time

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szChannelName;          // Channel name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szDownloadPath;         // Downloading path
            public DPSDK_RECORD_FILE_NAME_RULE iNameRule;       // Download file naming rules
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szDownloadFileName;     // Download the name of the file, if it is empty, use INameRuleThe defined rules are generated, not empty,Neglecting INameRule, szDownloadPath, szChannelNamefield
            public int iSplitFileSize;         // Division of file size, unit MB，0Non segmentation
            public DPSDK_DOWNLOAD_RECORD_FILE_FORMAT iFileFormat;// Download file format
        }

        // Download parameters by file
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DOWNLOAD_BY_FILE_PARAM
        {
            public DPSDK_EVENT_DOWNLOAD_CALLBACK fEventCallBack;// Event callbacks
            public IntPtr pEventUserData;         // Event callback user data

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;            // Channel ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceCode;           // Device code for request media key
            public DPSDK_SOURCE_TYPE iSourceType;            // Video source
            public Int64 tBeginTime;                // Start time
            public Int64 tEndTime;              // End time
            public UInt64 uSSId;                    // Storage service (IDReturn to the query)
            public UInt64 uFileHandle;			// File handle(Return to the query)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szDiskId;               // Disk ID(Return to the query)
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFilename;            // The name of the video (different manufacturers are different in the identification of the documents)

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szChannelName;          // Channel name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szDownloadPath;         // Downloading path
            public DPSDK_RECORD_FILE_NAME_RULE iNameRule;		// Download file naming rules
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szDownloadFileName;     // Download the name of the file, if it is empty, use INameRuleThe defined rules are generated, not empty,Neglecting INameRule, szDownloadPath, szChannelNamefield
            public int iSplitFileSize;          // Division of file size, unit MB，0Non segmentation
            public DPSDK_DOWNLOAD_RECORD_FILE_FORMAT iFileFormat;// Download file format
        }

        [StructLayout(LayoutKind.Sequential, Pack = 8)]
        public struct DPSDK_DOWNLOAD_RECORD_INFO
        {
            public int iDownloadID;
            public int iFileID;
            public int iDownloadMode;
            public int iRecordSource;
            public int iRecordType;
            public int iStreamType;
            public UInt64 uiCurFileSize;
            public UInt64 uiPrevFileSize;
            public Int64 tBeginTime;
            public Int64 tEndTime;
            public int iDownloadState;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;
            public UInt64 uiFileHandle;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string szDiskId;
            public int iDownloadStatus;
            public int iFileCount;
            public IntPtr szDownloadFileName;
        }

        // Picture turn BMP format
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CONVERT_BMP
        {
            public IntPtr pBuf;			        // Image data pointer
            public int lSize;			        // Image data size
            public int lWidth;			        // Image width
            public int lHeight;		        // Image height
            public int lType;			        // Image type
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szFileName;             // File name to be saved.It is best to BMP as a file extension
        }

        // Picture turn jpeg format
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CONVERT_JPEG
        {
            public IntPtr pBuf;			        // Image data pointer
            public int lSize;			        // Image data size
            public int lWidth;			        // Image width
            public int lHeight;		        // Image height
            public int lType;                   // Image type
            public int iQuality;		        // Image compression quality,region[0, 100]
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = DPSDK_FILE_PATH_LEN)]
            public string szFileName;             // File name to be saved.It is best to jpg as a file extension
        }

        //Event callback parameter structure
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_EVENT_PARAM
        {
            public int iSessionID;				// Conversation ID
            public IntPtr pBuf;					// Message structure
            public uint uiBufLen;				// Message structure length
        }

        // Screen shots callback structure
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_SCREENSHOT
        {
            public Int32 lPort;		            // Channel code
            public IntPtr pBuf;		            // The data of returned image
            public Int32 lSize;		            // User data
            public Int32 lWidth;		            // Image width, unit:pixel
            public Int32 lHeight;	            // Image height
            public Int32 lStamp;		            // The information of time scale, unit:ms
            public Int32 lType;		            // Data type,T_RGB32,T_UYVY
        }

        // Hierarchical acquisition of device tree request parameters
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_GET_DEVICE_LAYERED_PARAM
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szID;                   // Node ID，Represent one code of orgCode,deviceCode,channelCode
            public int iNodeType;               // DPSDK_NODE_TYPE definition 1:Organization,2:Equipment,3:passageway
            public int iOrgType;                // 1: Basic organization
            public int iShowDev;                // 0: No device nodes are needed,1: Need device node
            public int iDeep;                   // 2: organization+Equipment,3Organization+equipment+passageway

            public int iCategoryNum;			// Device large class list length
            public IntPtr pCategoryList;          // Device large list

            public int iChannelTypeNum;		// Channel type list length
            public IntPtr pChannelTypeList;       // Channel type set that needs to be querying See DPSDK_DEV_UNIT_TYPE Definition

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szKeyWord;              // Search keywords
        }

        // Paging information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_PAGE_INFO
        {
            public uint uiPage;                 // The current paging, from 1 start
            public uint uiPageSize;             // Page size
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Dev_Info_All
        {
            public ArrayList vecDevInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Dev_Info
        {
            public DPSDK_DEV_INFO struDevInfo;
            public ArrayList vecEncChnlInfo;
            public ArrayList vecDecChnlInfo;
            public ArrayList vecAlarmInChnlInfo;
            public ArrayList vecAlarmOutChnlInfo;
            public ArrayList vecTvWallInChnlInfo;
            public ArrayList vecTvWallOutChnlInfo;
            public ArrayList vecDoorChnlInfo;
            public ArrayList vecVoiceChnlInfo;
            public ArrayList vecRoadGateChnlInfo;
            public ArrayList vecLEDChnlInfo;
            public ArrayList vecDispatcherChnlInfo;
            public ArrayList vecPosChnlInfo;
            public ArrayList vecVirtualChnlInfo;
        }

        // Basic equipment information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEV_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceID;             // Device ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szDeviceName;           // Device name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szUserName;             // Device login user
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szUserPwd;              // Device login password
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szIP;                   // Device additionIP
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceSn;             // Device serial number
            public UInt16 ushPort;				// Device add port
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szDevIP;                // Device real ip
            public UInt16 ushDevPort;               // Device real port
            public int iManFac;             // Manufacturer
            public int iStatus;             // Device status	See DPSDK_DEV_STATUS Definition
            public int iDevType;				// Device type	see DPSDK_DEV_TYPE Definition
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;              // The organization code
            public int iDomainID;               // Domain ID
            public int iDevModel;               // Device model

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szSipId;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
            public string szSipPwd;
            public int iSipIdNum;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
            public DPSDK_CHANNEL_ID[] vthRelatedConfirmVTOSipId;
            public int iUnitEnable;
            public int iBuildingEnable;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szSoftwareVersion;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szHardwareVersion;
        }

        // Channel basic information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_BASE_CHANNEL_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelID;            // Channel ID
            public int iChannelSeq;			// Channel serial number, begin at 0
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szChannelName;          // Channel name
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szChnlSN;               // Channel SN code
            public int iChannelType;            // Channel type		see DPSDK_CHANNEL_TYPE  Only coded channels are currently available classification
            public int iStatus;             // Channel status	see DPSDK_DEV_STATUS
            public int iDomainID;				// Domain ID
        };

        // Channel extent information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_CHANNEL_EXT_INFO
        {
            public int iRoadway;                // Roadway 
            public float fCartMaxSpeed;			// Max speed of cart
            public float fCartMinSpeed;			// Min speed of cart
            public float fDollyMaxSpeed;			// Max speed of dolly
            public float fDollyMinSpeed;			// Min speed of dolly
            public float fDirection;				// Direction
        }

        // Coded channel information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ENC_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public DPSDK_CHANNEL_EXT_INFO struChnExtInfo;       // Channel extent information

            public int iCameraType;			// Camera type See DPSDK_CAMERA_TYPE Definition
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szLatitude;             // Latitude
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 50)]
            public string szLongitude;            // Longitude
            public int iCameraFunction;		// 0 No support function 1 Support fish eye 2 Support electric focusing
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szMulticastIP;          // Multicast IP
            public UInt16 ushMulticastPort;						// Multicast port
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szNVR_IPCIP;            // NVR Front end IPC IP
            public int iChannelRemoteType;      // Remote channel type See DPSDK_CHANNEL_REMOTE_TYPE Definition
            public int iFaceFunction;           // Face function, 0= not support, 1= snap, 2= identify
            public int iIntelliState;			// Intelligent status 0 off-ling,1 on-ling
            public int iTargetDetection;        // Intelligent status 0= not support

            // The type of unit in which the channel belongs
            public int iTrackID;                // Flow type
            public int iStreamType;         // Code stream type See DPSDK_STREAM_TYPE Definition
            public byte bZeroEncode;			// Does it support 0 Channel multi picture coding
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szKeyCode;              // Key code
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szDB33Code;             // DB code
            public int iPCFlag;				// 1=Area,2=In-Out,3=Out Door
        }

        // Decoding channel information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEC_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iMaxSpliteNum;           // Maximum division number equipment related

            // The type of unit in which the channel belongs
            public int iDecodeMode;         // Decoding mode See DPSDK_DECODE_MODE Definition
            public byte bConbineStatus;			// Does it support conbine
        };

        // Alarm input channel information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMIN_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iAlarmType;				// Alarm type
            public int iAlarmLevel;			// Alarm level
        }

        // Alarm output channel information
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ALARMOUT_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iAlarmType;				// Alarm type
        }

        // Large screen input channel data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_TVWALLIN_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iCameraType;			// Camera type See DPSDK_CAMERA_TYPEDefinition
            public int iChannelRemoteType;		// Remote channel type See DPSDK_CHANNEL_REMOTE_TYPEDefinition
        }

        // Large screen output channel data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_TVWALLOUT_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iDecodeMode;			// Decoding mode See DPSDK_DECODE_MODEDefinition
        };

        // Access data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DOOR_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;

            // Unit attributes of a channel
            public int iThirdControl;			// Whether third party control is allowed 0 no 1 yes
        }

        // Voice Channel Data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_VOICE_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;

            // Channel Cell Attribute
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szVoiceIP;              // Voice Service Address
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szClientIP;             // Voice Client Address
            public UInt16 ushVoicePort;			// Voice Service Port
            public UInt16 ushStatusPort;			// Voice Status Port
        }

        // Channel gate data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ROADGATE_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szSluiceType;           // Channel gate type
        }

        // LED Channel data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LED_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            public int iFreeParkingSpace;						// Residual parking space
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szLEDChnlDesc;          // Description information
        }

        // Dispatcher channel data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DISPATCHER_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 20)]
            public string szCallNum;              // Phone number
        }

        // POS Channel data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_POS_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szLinkChnl;             // POS Channel binding video source
        }

        // Virtual Channel Data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_VIRTUAL_CHANNEL_INFO
        {
            public DPSDK_BASE_CHANNEL_INFO struChannelInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct Dep_Info_All
        {
            public Dep_Info depInfo;			    // Org data
            public ArrayList vecSubDepInfo;          // Sub org list
            public ArrayList vecDevID;			    // Sub device list
            public ArrayList vecChnlID;			    // Sub channel list
        }

        // Department information
        [StructLayout(LayoutKind.Sequential)]
        public struct Dep_Info
        {
            public string strCoding;                // Node code
            public string strDepName;               // Node name
            public string strSN;                    // 
            public int nDepType;                // Org node type
            public int nDepSort;                // Org sort
            public byte isParent;
        }

        // Organization device condition
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERY_DEV_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;              // Organization code
            public uint uiCategoriesCount;		// The number of device categories which are need to query
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
            public int[] iCategoriesList;        // The device categories function which are need to query see DPSDK_DEV_UNIT_TYPE
            public IntPtr struDevIdList;			// The device id list which are need to query
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_QUERY_ORG_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szOrgCode;				// Oorganization code is the length which is the default query root organization
            public IntPtr iChannelTypeList;		// Channel type set that needs to be querying see DPSDK_DEV_UNIT_TYPEDefinition
        }

        // Channel code
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEVICE_ID
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szId;                   // Device id
        }

        // Obtain Layered List of Device Tree Returned Result
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LAYERED_RESULT_LIST
        {
            public int iResultNum;              // List Length
            public IntPtr pResultList;			// Result List
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ORG_SUB_DEV_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szDeviceId;	                        // Device ID
            public int iSort;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ORG_SUB_CHANNEL_INFO
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szChannelId;		                // Channel ID
            public int iSort;								// Sort
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_ORG_INFO
        {
            public DPSDK_ORG_BASE_INFO struOrgBaseInfo;			// Organization Info
            public int iDevNum;									// Number of Child Device
            public IntPtr pDevList;			                    // List of Child Device
            public int iChannelNum;								// Number of Sub-channel
            public IntPtr pChannelList;	                        // List of Sub-channel
            public int iOrgNum;									// Number of Sub-organization
            public IntPtr pOrgList;								// List of Sub-organization
        }

        // Gradation gets the result of the device tree
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_LAYERED_RESULT
        {
            public int iNodeType;				// See DPSDK_NODE_TYPE Definition 1:Organization,2:Equipment,3:passageway

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szID;                   // Node ID
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public string szName;                 // Node name
            public byte isParent;				// Whether it is a parent node
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 96)]
            public string szParentID;             // Parent node ID
            public int iSort;                   // Sort value
            public int iStatus;             // Channel state see DPSDK_DEV_STATUS Definition

            public int iType1;                  // iNodeType For equipment, it represents a large class of equipment.INodeTypeUnit type for channel time
            public int iType2;					// iNodeType A small class of devices is represented when the device is used.INodeTypeExpress channel type for channel
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 56)]
            public string szSN;                   // SN code
                                                  // Equipment information
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
            public string szIP;                   // Device IP
                                                  // Channel information
            public int iChannelSeq;         // Channel code
            public int iDomainID;				// Domain  ID
        }

        // Device list
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEV_ALL_INFO_LIST
        {
            public int iDevNum;             // Number of devices
            public IntPtr pDevAllInfoList;		// Device list data
        }

        // Device data
        [StructLayout(LayoutKind.Sequential)]
        public struct DPSDK_DEV_ALL_INFO
        {
            public DPSDK_DEV_INFO struDevInfo;          // Device data 

            // Coding channel
            public int iEncChnlNum;			// Number of coded channels 
            public IntPtr pEncChnlInfoList;     // Code channel list 
                                                // Decoding channel
            public int iDecChnlNum;			// Decode channel number 
            public IntPtr pDecChnlInfoList;     // Decode channel list 
                                                // Alarm input channel
            public int iAlarmInChnlNum;		// Number of alarm input 
            public IntPtr pAlarmInChnlInfoList; // Alarm input list 
                                                // Alarm output channel
            public int iAlarmOutChnlNum;		// Alarm output number 
            public IntPtr pAlarmOutChnlInfoList;    // Alarm output list 
                                                    // Large screen input channel
            public int iTvWallInChnlNum;		// Large screen input channel number 
            public IntPtr pTvWallInChnlInfoList;	// Large screen input channel list 

            public int iTvWallOutChnlNum;		// Large screen output channel number 
            public IntPtr pTvWallOutChnlInfoList;   // Large screen output channel list 
                                                    // Entrance guard channel
            public int iDoorChnlNum;			// Number of access channels 
            public IntPtr pDoorChnlInfoList;        // List of access channels 
                                                    // Voice channel
            public int iVoiceChnlNum;			// Voice channel number 
            public IntPtr pVoiceChnlInfoList;       // Voice channel list 
                                                    // Channel gate 
            public int iRoadGateChnlNum;		// Number of channel gates 
            public IntPtr pRoadGateChnlInfoList;    // List of channel gates 
                                                    // LED channel
            public int iLEDChnlNum;			// LED Number of channels 
            public IntPtr pLEDChnlInfoList;     // LED Channel list 
                                                // Dispatcher channel
            public int iDispatcherChnlNum;		// Number of channels for the dispatcher 
            public IntPtr pDispatcherChnlInfoList;// Scheduler list 
                                                  // POS channel
            public int iPosChnlNum;			// POS Number of channels 
            public IntPtr pPosChnlInfoList;     // POS Channel list 
                                                // Virtual channel
            public int iVirtualChnlNum;		// Number of virtual channels  
            public IntPtr pVirtualChnlInfoList;	// Virtual channel list

            // Event detetor channel
            //public int                  iDetetorChnlNum;		// Number of evnet detetor channels  
            //public IntPtr               pDetetorChnlInfoList;	// Event detetor channel list
        }
    }
}
