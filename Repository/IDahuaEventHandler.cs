﻿using System;
using System.Threading.Tasks;

namespace dahua.Repository
{
    public interface IDahuaEventHandler
    {
        void eventCallback(IntPtr iEventType, IntPtr pEventBuf, IntPtr uiBufSize, IntPtr pUserData);
        Task sendMessage(string command, string user, string message);
    }
}