﻿using dahua.Model;
using dahua.Repository;
using dahua.Util;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using static dahua.Repository.DahuaRepository;
using DPSDK_RES = System.Int32;

namespace dahua.Service
{
    public class DahuaService : IDahuaService
    {
        private readonly ILogger<DahuaService> _logger;
        private readonly ICopyUtil _copyUtil;
        private readonly IDahuaEventHandler _dahuaEventHandler;
        private IntPtr sessionId;
        private Dev_Info_All devicesInfo;
        private List<Device> devicesList;
        public EventCallBack eventCallback;

        private string DahuaUrl { get; set; }
        private uint DahuaPort { get; set; }
        private string DahuaUsername { get; set; }
        private string DahuaPassword { get; set; }

        public DahuaService(IConfiguration configuration, ILogger<DahuaService> logger, ICopyUtil copyUtil, IDahuaEventHandler dahuaEventHandler)
        {
            this._logger = logger;
            this._copyUtil = copyUtil;
            this._dahuaEventHandler = dahuaEventHandler;
            DahuaUrl = configuration.GetSection("Dahua").GetSection("URL").Value;
            DahuaPort = Convert.ToUInt32(configuration.GetSection("Dahua").GetSection("Port").Value);
            DahuaUsername = configuration.GetSection("Dahua").GetSection("Username").Value;
            DahuaPassword = configuration.GetSection("Dahua").GetSection("Password").Value;
            eventCallback = this._dahuaEventHandler.eventCallback;
        }

        public bool config()
        {
            _logger.LogDebug("INIT DAHUA SERVER - START");
            DPSDK_SetLogInfo("log\\DPSDK_LOG", (IntPtr)DPSDK_LOG_LEVEL_TYPE.LOG_LEVEL_INFO);
            if (DPSDK_Init() == DPSDK_SUCCESS)
            {
                _logger.LogDebug("Init system success!");
                DPSDK_LOGIN_PARAM loginParam = new DPSDK_LOGIN_PARAM();
                loginParam.bDomainUser = (byte)0; //Must set this to 0. Otherwise, will get LoginSecTime Error
                loginParam.szUserName = DahuaUsername;
                loginParam.szPWD = DahuaPassword;
                loginParam.struIP.szIP = DahuaUrl;
                loginParam.uiPort = DahuaPort;
                loginParam.uiClientType = 1;
                DPSDK_RES returnCode = DPSDK_Login(ref loginParam, ref sessionId);
                if (returnCode == DPSDK_SUCCESS)
                {
                    _logger.LogInformation("Login successful!");
                    eventCallback = _dahuaEventHandler.eventCallback;
                    DPSDK_RES returnCallback = DPSDK_SetEventCallBack(sessionId, eventCallback, /*IntPtr.Zero*/new IntPtr(1));
                    _logger.LogDebug("Setting event callback! Return : " + returnCallback);
                    return true;
                }
                _logger.LogError("Login failed. error code = " + returnCode.ToString());
                return false;
            }
            _logger.LogError("Init system failed!");
            return false;
        }

        public List<Device> getDevice()
        {
            devicesInfo = new Dev_Info_All();
            devicesInfo.vecDevInfo = new ArrayList();
            devicesList = new List<Device>();

            _logger.LogDebug("sessionId : " + (int) sessionId);
            DPSDK_RES returnCode = DPSDK_GetDevice(sessionId, IntPtr.Zero, 0, getDeviceCallBack, IntPtr.Zero);
            if (returnCode == DPSDK_SUCCESS)
            {
                _logger.LogDebug("Get Device.");
                return devicesList;
            }
            _logger.LogError("Get Device Failed. error code = " + returnCode.ToString());
            return new List<Device>();
        }

        private void getDeviceCallBack(IntPtr iDataType, IntPtr pDataBuf, IntPtr uiBufSize, IntPtr pUserData)
        {
            if (iDataType.ToInt32() == DPSDK_DATA_ORG_INFO)
            {
                DPSDK_ORG_INFO pOrgInfo = (DPSDK_ORG_INFO)Marshal.PtrToStructure(pDataBuf, typeof(DPSDK_ORG_INFO));
                //CopyOrgData(ref pOrgInfo, ref m_struDepInfoAll);
            }
            else if (iDataType.ToInt32() == DPSDK_DATA_ALL_ORG_INFO)
            {

            }
            else if (iDataType.ToInt32() == DPSDK_DATA_DEVICE_INFO)
            {
                DPSDK_DEV_ALL_INFO_LIST p_devicesInfo = (DPSDK_DEV_ALL_INFO_LIST)Marshal.PtrToStructure(pDataBuf, typeof(DPSDK_DEV_ALL_INFO_LIST));
                _copyUtil.copyDevicesData(ref p_devicesInfo, ref devicesInfo, ref devicesList);
            }
            else if (iDataType.ToInt32() == DPSDK_DATA_DEVICE_LAYERED)
            {
                DPSDK_LAYERED_RESULT_LIST pSrcInfo = (DPSDK_LAYERED_RESULT_LIST)Marshal.PtrToStructure(pDataBuf, typeof(DPSDK_LAYERED_RESULT_LIST));
                string pParentId = Marshal.PtrToStringAnsi(pUserData);
                //FinOrgAndCopyDevTree(pParentId, ref pSrcInfo, ref m_struDepInfoAll);
            }
        }

        public AlarmCount getAlarmCount()
        {
            DPSDK_QUERYALARMCOUNT_PARAM alarmParam = new DPSDK_QUERYALARMCOUNT_PARAM();
            alarmParam.szAlarmCode = "";
            alarmParam.szAlarmId = "";
            alarmParam.iChannelIdNum = 1;
            alarmParam.pChannelIdList = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(DPSDK_CHANNEL_ID)));
            
            DPSDK_CHANNEL_ID channelParam = new DPSDK_CHANNEL_ID();
            channelParam.szId = "";
            Marshal.StructureToPtr(channelParam, alarmParam.pChannelIdList, true);

            alarmParam.iDeviceIdNum = 1;
            alarmParam.pDeviceIdList = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(DPSDK_DEVICE_ID)));

            DPSDK_DEVICE_ID deviceParam = new DPSDK_DEVICE_ID();
            deviceParam.szId = "";
            Marshal.StructureToPtr(deviceParam, alarmParam.pDeviceIdList, true);

            alarmParam.szOrgId = "";
            alarmParam.szHandleUser = "";
            alarmParam.szBeginTime = "";
            alarmParam.szEndTime = "";
            alarmParam.szHandleBeginTime = "";
            alarmParam.szHandleEndTime = "";

            // AlarmTypeList
            string alarmType = "";
            ArrayList arrAlarmType = new ArrayList();
            _copyUtil.stringSplit(alarmType, ";", ref arrAlarmType);
            alarmParam.uiAlarmTypeNumber = (uint)arrAlarmType.Count;
            if (arrAlarmType.Count > 0)
            {
                alarmParam.pAlarmType = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmType.Count);
                for (int i = 0; i < arrAlarmType.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmType.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmType[i]), ptr, true);
                }
            }

            // AlarmGradeList
            string alarmGrade = "";
            ArrayList arrAlarmGrade = new ArrayList();
            _copyUtil.stringSplit(alarmGrade, ";", ref arrAlarmGrade);
            alarmParam.uiAlarmGradeNumber = (uint)arrAlarmGrade.Count;
            if (arrAlarmGrade.Count > 0)
            {
                alarmParam.pAlarmGrade = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmGrade.Count);
                for (int i = 0; i < arrAlarmGrade.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmGrade.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmGrade[i]), ptr, true);
                }
            }

            // AlarmStatusList
            string alarmStatus = "";
            ArrayList arrAlarmStatus = new ArrayList();
            _copyUtil.stringSplit(alarmStatus, ";", ref arrAlarmStatus);
            alarmParam.uiAlarmStatusNumber = (uint)arrAlarmStatus.Count;
            if (arrAlarmStatus.Count > 0)
            {
                alarmParam.pAlarmStatus = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmStatus.Count);
                for (int i = 0; i < arrAlarmStatus.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmStatus.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmStatus[i]), ptr, true);
                }
            }

            // HandleStatusList
            string alarmHandleStatus = "";
            ArrayList arrAlarmHandleStatus = new ArrayList();
            _copyUtil.stringSplit(alarmHandleStatus, ";", ref arrAlarmHandleStatus);
            alarmParam.uiHandleStatusNumber = (uint)arrAlarmHandleStatus.Count;
            if (arrAlarmHandleStatus.Count > 0)
            {
                alarmParam.pHandleStatus = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmHandleStatus.Count);
                for (int i = 0; i < arrAlarmHandleStatus.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pHandleStatus.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmHandleStatus[i]), ptr, true);
                }
            }

            IntPtr alarmCount = IntPtr.Zero;
            DPSDK_RES iRet = DPSDK_QueryAlarmCount(sessionId, ref alarmParam, ref alarmCount);
            if (iRet == DPSDK_SUCCESS)
            {
                _logger.LogDebug("QueryAlarmCount Success,alarm count= " + alarmCount.ToString());
            }
            else
            {
                _logger.LogError("QueryAlarmCount failed. error code = " + iRet.ToString());
            }

            Marshal.FreeHGlobal(alarmParam.pChannelIdList);
            Marshal.FreeHGlobal(alarmParam.pDeviceIdList);
            if (arrAlarmType.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmType);
            }
            if (arrAlarmGrade.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmGrade);
            }
            if (arrAlarmStatus.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmStatus);
            }
            if (arrAlarmHandleStatus.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pHandleStatus);
            }

            return new AlarmCount()
            {
                Count = (int)alarmCount
            };
        }

        public List<Alarm> getAlarm()
        {
            DPSDK_QUERYALARM_PARAM alarmParam = new DPSDK_QUERYALARM_PARAM();
            alarmParam.iPageNo = 1;
            alarmParam.iPageSize = 10;
            alarmParam.iSortType = 1;
            alarmParam.iSortOrder = 0;
            alarmParam.szAlarmCode = "";
            alarmParam.szAlarmId = "";
            alarmParam.iChannelIdNum = 1;
            alarmParam.pChannelIdList = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(DPSDK_CHANNEL_ID)));
            
            DPSDK_CHANNEL_ID channelId = new DPSDK_CHANNEL_ID();
            channelId.szId = "";
            Marshal.StructureToPtr(channelId, alarmParam.pChannelIdList, true);

            alarmParam.iDeviceIdNum = 1;
            alarmParam.pDeviceIdList = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(DPSDK_DEVICE_ID)));

            DPSDK_DEVICE_ID deviceId = new DPSDK_DEVICE_ID();
            deviceId.szId = "";
            Marshal.StructureToPtr(deviceId, alarmParam.pDeviceIdList, true);

            alarmParam.szOrgId = "";
            alarmParam.szHandleUser = "";
            alarmParam.szBeginTime = "";
            alarmParam.szEndTime = "";
            alarmParam.szHandleBeginTime = "";
            alarmParam.szHandleEndTime = "";

            // AlarmTypeList
            string alarmType = "";
            ArrayList arrAlarmType = new ArrayList();
            _copyUtil.stringSplit(alarmType, ";", ref arrAlarmType);
            alarmParam.uiAlarmTypeNumber = (uint)arrAlarmType.Count;
            if (arrAlarmType.Count > 0)
            {
                alarmParam.pAlarmType = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmType.Count);
                for (int i = 0; i < arrAlarmType.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmType.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmType[i]), ptr, true);
                }
            }

            // AlarmGradeList
            string alarmGrade = "";
            ArrayList arrAlarmGrade = new ArrayList();
            _copyUtil.stringSplit(alarmGrade, ";", ref arrAlarmGrade);
            alarmParam.uiAlarmGradeNumber = (uint)arrAlarmGrade.Count;
            if (arrAlarmGrade.Count > 0)
            {
                alarmParam.pAlarmGrade = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmGrade.Count);
                for (int i = 0; i < arrAlarmGrade.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmGrade.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmGrade[i]), ptr, true);
                }
            }

            // AlarmStatusList
            string alarmStatus = "";
            ArrayList arrAlarmStatus = new ArrayList();
            _copyUtil.stringSplit(alarmStatus, ";", ref arrAlarmStatus);
            alarmParam.uiAlarmStatusNumber = (uint)arrAlarmStatus.Count;
            if (arrAlarmStatus.Count > 0)
            {
                alarmParam.pAlarmStatus = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmStatus.Count);
                for (int i = 0; i < arrAlarmStatus.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pAlarmStatus.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmStatus[i]), ptr, true);
                }
            }

            // HandleStatusList
            string alarmHandleStatus ="";
            ArrayList arrAlarmHandleStatus = new ArrayList();
            _copyUtil.stringSplit(alarmHandleStatus, ";", ref arrAlarmHandleStatus);
            alarmParam.uiHandleStatusNumber = (uint)arrAlarmHandleStatus.Count;
            if (arrAlarmHandleStatus.Count > 0)
            {
                alarmParam.pHandleStatus = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(int)) * arrAlarmHandleStatus.Count);
                for (int i = 0; i < arrAlarmHandleStatus.Count; i++)
                {
                    IntPtr ptr = new IntPtr(alarmParam.pHandleStatus.ToInt32() + Marshal.SizeOf(typeof(int)) * i);
                    Marshal.StructureToPtr(Convert.ToInt32((string)arrAlarmHandleStatus[i]), ptr, true);
                }
            }

            uint uiBufLen = 0;
            if (alarmParam.iPageSize > 1)
            {
                //Since C++ struct byte alignment£¬it is not need to sub IntPtr size
                uiBufLen = (uint)(Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO_LIST))/* - Marshal.SizeOf(typeof(IntPtr))*/ + Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO)) * alarmParam.iPageSize);
            }
            else
            {
                uiBufLen = (uint)(Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO_LIST))/* - Marshal.SizeOf(typeof(IntPtr))*/ + Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO)));
            }
            IntPtr pAlarmDetailInfoList = Marshal.AllocHGlobal((int)uiBufLen);

            List<Alarm> alarmList = new List<Alarm>();
            DPSDK_RES iRet = DPSDK_QueryAlarm(sessionId, ref alarmParam, uiBufLen, pAlarmDetailInfoList);
            if (iRet == DPSDK_SUCCESS)
            {
                DPSDK_ALARM_DETAILINFO_LIST alarmInfoList = new DPSDK_ALARM_DETAILINFO_LIST();
                alarmInfoList = (DPSDK_ALARM_DETAILINFO_LIST)Marshal.PtrToStructure(pAlarmDetailInfoList, typeof(DPSDK_ALARM_DETAILINFO_LIST));
                for (int i = 0; i < (int)alarmInfoList.uiTotal; i++)
                {
                    DPSDK_ALARM_DETAILINFO alarmInfo = new DPSDK_ALARM_DETAILINFO();
                    IntPtr ptr = new IntPtr(pAlarmDetailInfoList.ToInt32() + Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO_LIST)) + Marshal.SizeOf(typeof(DPSDK_ALARM_DETAILINFO)) * i);
                    alarmInfo = (DPSDK_ALARM_DETAILINFO)Marshal.PtrToStructure(ptr, typeof(DPSDK_ALARM_DETAILINFO));

                    Alarm alarm = new Alarm()
                    {
                        AlarmId = alarmInfo.szAlarmId,
                        DeviceId = alarmInfo.szDeviceId,
                        DeviceName = alarmInfo.szDeviceName,
                        ChannelId = alarmInfo.szChannelId,
                        ChannelName = alarmInfo.szChannelName,
                        HandleUser = alarmInfo.szHandleUser,
                        HandleTime = alarmInfo.szHandleTime,
                        HandleMessage = alarmInfo.szHandleMessage,
                        AlarmCode = alarmInfo.szAlarmCode,
                        AlarmTime = alarmInfo.szAlarmTime,
                        AlarmGrade = alarmInfo.iAlarmGrade.ToString(),
                        AlarmType = alarmInfo.iAlarmType.ToString(),
                        AlarmStatus = alarmInfo.iAlarmStatus.ToString(),
                        HandleStatus = alarmInfo.iHandleStatus.ToString(),
                        AlarmPicture = alarmInfo.szAlarmPicture,
                        AlarmPictureSize = alarmInfo.uiAlarmPictureSize.ToString()
                    };
                    alarmList.Add(alarm);
                }
            }
            else
            {
                _logger.LogError("AlarmQuery failed. error code = " + iRet.ToString() + ".\r\n");
            }

            Marshal.FreeHGlobal(alarmParam.pChannelIdList);
            Marshal.FreeHGlobal(alarmParam.pDeviceIdList);
            if (arrAlarmType.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmType);
            }
            if (arrAlarmGrade.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmGrade);
            }
            if (arrAlarmStatus.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pAlarmStatus);
            }
            if (arrAlarmHandleStatus.Count > 0)
            {
                Marshal.FreeHGlobal(alarmParam.pHandleStatus);
            }
            Marshal.FreeHGlobal(pAlarmDetailInfoList);

            return alarmList;
        }

        public string getAlarmType()
        {
            string language = "en_US";
            IntPtr alarmType = IntPtr.Zero;
            DPSDK_RES iRet = DPSDK_GetAlarmTypeGroupInfo(sessionId, language, ref alarmType);
            if (iRet == DPSDK_SUCCESS)
            {
                if (alarmType != IntPtr.Zero)
                {
                    byte[] strAlarmTypeXml = new byte[30000];
                    Marshal.Copy(alarmType, strAlarmTypeXml, 0, 30000);
                    _logger.LogDebug(Encoding.GetEncoding("utf-8").GetString(strAlarmTypeXml));
                }
            }
            else
            {
                _logger.LogError("GetAlarmType failed. error code = " + iRet.ToString());
            }
            DPSDK_ReleaseDataBuffer(alarmType);
            return "Hello World";
        }
    }
}
