﻿using dahua.Model;
using System.Collections.Generic;

namespace dahua.Service
{
    public interface IDahuaService
    {
        List<Device> getDevice();
        AlarmCount getAlarmCount();
        List<Alarm> getAlarm();
        string getAlarmType();
        bool config();
    }
}
