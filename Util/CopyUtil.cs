﻿using dahua.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using static dahua.Repository.DahuaRepository;

namespace dahua.Util
{
    public class CopyUtil : ICopyUtil
    {
        private readonly ILogger<CopyUtil> _logger;

        public CopyUtil(ILogger<CopyUtil> logger)
        {
            this._logger = logger;
        }
        public void copyDevicesData(ref DPSDK_DEV_ALL_INFO_LIST p_devicesInfo, ref Dev_Info_All devicesInfo, ref List<Device> devicesList)
        {
            for (int i = 0; i < p_devicesInfo.iDevNum; i++)
            {
                Dev_Info devInfo = new Dev_Info();
                devInfo.vecEncChnlInfo = new ArrayList();
                devInfo.vecDecChnlInfo = new ArrayList();
                devInfo.vecAlarmInChnlInfo = new ArrayList();
                devInfo.vecAlarmOutChnlInfo = new ArrayList();
                devInfo.vecTvWallInChnlInfo = new ArrayList();
                devInfo.vecTvWallOutChnlInfo = new ArrayList();
                devInfo.vecDoorChnlInfo = new ArrayList();
                devInfo.vecVoiceChnlInfo = new ArrayList();
                devInfo.vecRoadGateChnlInfo = new ArrayList();
                devInfo.vecLEDChnlInfo = new ArrayList();
                devInfo.vecDispatcherChnlInfo = new ArrayList();
                devInfo.vecPosChnlInfo = new ArrayList();
                devInfo.vecVirtualChnlInfo = new ArrayList();

                IntPtr ptr = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)));
                DPSDK_DEV_ALL_INFO p_devInfo = (DPSDK_DEV_ALL_INFO)Marshal.PtrToStructure(ptr, typeof(DPSDK_DEV_ALL_INFO));
                devInfo.struDevInfo = p_devInfo.struDevInfo;

                Device device = new Device()
                {
                    Id = devInfo.struDevInfo.szDeviceID,
                    Name = devInfo.struDevInfo.szDeviceName,
                    Type = devInfo.struDevInfo.iDevType,
                    IP = devInfo.struDevInfo.szDevIP,
                    Port = devInfo.struDevInfo.ushDevPort,
                    Status = devInfo.struDevInfo.iStatus
                };
                devicesList.Add(device);

                for (int j = 0; j < p_devInfo.iEncChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 1 * Marshal.SizeOf(typeof(int)) + 0 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrEncChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_ENC_CHANNEL_INFO struEncChnlInfo = (DPSDK_ENC_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrEncChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_ENC_CHANNEL_INFO))), typeof(DPSDK_ENC_CHANNEL_INFO));
                    devInfo.vecEncChnlInfo.Add(struEncChnlInfo);
                    _logger.LogDebug("1 : " + struEncChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iDecChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 2 * Marshal.SizeOf(typeof(int)) + 1 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrDecChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_DEC_CHANNEL_INFO struDecChnlInfo = (DPSDK_DEC_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrDecChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_DEC_CHANNEL_INFO))), typeof(DPSDK_DEC_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struDecChnlInfo);
                    _logger.LogDebug("2 : " + struDecChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iAlarmInChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 3 * Marshal.SizeOf(typeof(int)) + 2 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrAlarmInChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_ALARMIN_CHANNEL_INFO struAlarmInChnlInfo = (DPSDK_ALARMIN_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrAlarmInChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_ALARMIN_CHANNEL_INFO))), typeof(DPSDK_ALARMIN_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struAlarmInChnlInfo);
                    _logger.LogDebug("3 : " + struAlarmInChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iAlarmOutChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 4 * Marshal.SizeOf(typeof(int)) + 3 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrAlarmOutChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_ALARMOUT_CHANNEL_INFO struAlarmOutChnlInfo = (DPSDK_ALARMOUT_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrAlarmOutChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_ALARMOUT_CHANNEL_INFO))), typeof(DPSDK_ALARMOUT_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struAlarmOutChnlInfo);
                    _logger.LogDebug("4 : " + struAlarmOutChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iTvWallInChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 5 * Marshal.SizeOf(typeof(int)) + 4 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrTvWallInChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_TVWALLIN_CHANNEL_INFO struTvWallInChnlInfo = (DPSDK_TVWALLIN_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrTvWallInChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_TVWALLIN_CHANNEL_INFO))), typeof(DPSDK_TVWALLIN_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struTvWallInChnlInfo);
                    _logger.LogDebug("5 : " + struTvWallInChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iTvWallOutChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 6 * Marshal.SizeOf(typeof(int)) + 5 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrTvWallOutChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_TVWALLOUT_CHANNEL_INFO struTvWallOutChnlInfo = (DPSDK_TVWALLOUT_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrTvWallOutChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_TVWALLOUT_CHANNEL_INFO))), typeof(DPSDK_TVWALLOUT_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struTvWallOutChnlInfo);
                    _logger.LogDebug("6 : " + struTvWallOutChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iDoorChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 7 * Marshal.SizeOf(typeof(int)) + 6 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrDoorChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_DOOR_CHANNEL_INFO struDoorChnlInfo = (DPSDK_DOOR_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrDoorChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_DOOR_CHANNEL_INFO))), typeof(DPSDK_DOOR_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struDoorChnlInfo);
                    _logger.LogDebug("7 : " + struDoorChnlInfo.struChannelInfo.szChannelName);
                }

                for (int j = 0; j < p_devInfo.iVoiceChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 8 * Marshal.SizeOf(typeof(int)) + 7 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrVoiceChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_VOICE_CHANNEL_INFO struVoiceChnlInfo = (DPSDK_VOICE_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrVoiceChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_VOICE_CHANNEL_INFO))), typeof(DPSDK_VOICE_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struVoiceChnlInfo);
                    _logger.LogDebug("8 : " + struVoiceChnlInfo.struChannelInfo.szChannelName);
                }
                for (int j = 0; j < p_devInfo.iRoadGateChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 9 * Marshal.SizeOf(typeof(int)) + 8 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrRoadGateChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_ROADGATE_CHANNEL_INFO struRoadGateChnlInfo = (DPSDK_ROADGATE_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrRoadGateChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_ROADGATE_CHANNEL_INFO))), typeof(DPSDK_ROADGATE_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struRoadGateChnlInfo);
                    _logger.LogDebug("9 : " + struRoadGateChnlInfo.struChannelInfo.szChannelName);
                }
                for (int j = 0; j < p_devInfo.iLEDChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 10 * Marshal.SizeOf(typeof(int)) + 9 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrLEDChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_LED_CHANNEL_INFO struLEDChnlInfo = (DPSDK_LED_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrLEDChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_LED_CHANNEL_INFO))), typeof(DPSDK_LED_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struLEDChnlInfo);
                    _logger.LogDebug("10 : " + struLEDChnlInfo.struChannelInfo.szChannelName);
                }
                for (int j = 0; j < p_devInfo.iDispatcherChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 11 * Marshal.SizeOf(typeof(int)) + 10 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrDispatcherChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_DISPATCHER_CHANNEL_INFO struDispatcherChnlInfo = (DPSDK_DISPATCHER_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrDispatcherChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_DISPATCHER_CHANNEL_INFO))), typeof(DPSDK_DISPATCHER_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struDispatcherChnlInfo);
                    _logger.LogDebug("11 : " + struDispatcherChnlInfo.struChannelInfo.szChannelName);
                }
                for (int j = 0; j < p_devInfo.iPosChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 12 * Marshal.SizeOf(typeof(int)) + 11 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrPosChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_POS_CHANNEL_INFO struPosChnlInfo = (DPSDK_POS_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrPosChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_POS_CHANNEL_INFO))), typeof(DPSDK_POS_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struPosChnlInfo);
                    _logger.LogDebug("12 : " + struPosChnlInfo.struChannelInfo.szChannelName);
                }
                for (int j = 0; j < p_devInfo.iVirtualChnlNum; j++)
                {
                    IntPtr ptrChnl = new IntPtr(p_devicesInfo.pDevAllInfoList.ToInt32() + i * Marshal.SizeOf(typeof(DPSDK_DEV_ALL_INFO)) + Marshal.SizeOf(typeof(DPSDK_DEV_INFO)) + 13 * Marshal.SizeOf(typeof(int)) + 12 * Marshal.SizeOf(typeof(IntPtr)));
                    IntPtr ptrVirtualChnlInfoList = (IntPtr)Marshal.PtrToStructure(ptrChnl, typeof(IntPtr));
                    DPSDK_VIRTUAL_CHANNEL_INFO struVirtualChnlInfo = (DPSDK_VIRTUAL_CHANNEL_INFO)Marshal.PtrToStructure(new IntPtr(ptrVirtualChnlInfoList.ToInt32() + j * Marshal.SizeOf(typeof(DPSDK_VIRTUAL_CHANNEL_INFO))), typeof(DPSDK_VIRTUAL_CHANNEL_INFO));
                    devInfo.vecDecChnlInfo.Add(struVirtualChnlInfo);
                    _logger.LogDebug("13 : " + struVirtualChnlInfo.struChannelInfo.szChannelName);
                }
                devicesInfo.vecDevInfo.Add(devInfo);
            }
        }

        public void stringSplit(string src, string sep, ref ArrayList dest)
        {
            string str = src;
            string subStr = "";
            int start = 0, index = 0;

            do
            {
                index = str.IndexOf(sep, start);
                if (index != -1)
                {
                    subStr = str.Substring(start, index - start);
                    if (subStr.Length > 0 && subStr != sep)
                    {
                        dest.Add(subStr);
                    }
                    start = index + sep.Length;
                }
            } while (index != -1);

            subStr = str.Substring(start);
            if (subStr.Length > 0 && subStr != sep)
            {
                dest.Add(subStr);
            }
        }
    }
}
