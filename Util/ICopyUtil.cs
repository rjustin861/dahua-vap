﻿using dahua.Model;
using dahua.Repository;
using System.Collections;
using System.Collections.Generic;

namespace dahua.Util
{
    public interface ICopyUtil
    {
        void copyDevicesData(ref DahuaRepository.DPSDK_DEV_ALL_INFO_LIST p_devicesInfo, ref DahuaRepository.Dev_Info_All devicesInfo, ref List<Device> devicesList);
        void stringSplit(string src, string sep, ref ArrayList dest);
    }
}